LLRPDB                                                          Stuart Lynne
Wimsey                                          Tue Nov 04 22:43:36 PST 2014 




Installation Prerequisites.
**************************

apt-get
    install build-essential
    libxml-libxslt-perl


cpan
    YAML
    DBI
    Linux::Socket
    Time::CTime
    DateTime (N.b. may need cpan -fi to force)
    DateTime::Format::Strptime
    Data::HexDump
    XML::LibXSLT
    Text::CSV
    DBD::SQLite
    


RaceSync
********

Use ssh-keygen to generate a non password protected id_rsa and id_rsa.pub files.

Append contents of id_rsa.pub to ~workouts/.ssh/authorized_keys on the target
host.


NTP
###

Ensure that NTP is installed:

    apt-get install ntp

N.B. 2014 - ntp.conf contains N.pool.ubuntu.ntp.org, these do not work. Simply
change to N.pool.ntp.org.


Impinj NTP
**********

The LLRPDB script assumes that the readers are reasonably close to wall clock
time.

The best way to achieve this is to ensure that they are configured to use NTP.

For our application we typically configure them on a restricted local network.
E.g.:

    10.16.31.82

While the local system may be something like:

    10.0.0.15


Use rshell:

    telnet 10.16.31.82
    root
    impinj
    show network summary
    show network ntp
    config network ntp
    add 10.0.0.15
    show network ntp
    exit



