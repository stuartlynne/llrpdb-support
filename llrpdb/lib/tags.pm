

package tags;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK @EXPORT_TAGS);
use Data::Dumper;



$VERSION        = 1.00;
@ISA            = qw(Exporter);
@EXPORT         = qw(record_tag lapdcsvheader infocsvheader);

use Sys::Syslog qw (:standard :macros);

use lib::gmoffset qw(getlocaloffset getgmtoffset);
use lib::tags qw(lapdheader);

my $DEBUG = 0;

my $SCALE = (138/200);

# ################################################################################################################### #
# convert a timestamp hh:mm::ss + milliseconds to the total number of milliseconds.
#
sub get_ms {
    my ($xtime, $mstime) = @_;
    my @values = split(/\./,$xtime);
    return 0 unless (looks_like_number($values[0]));
    return 0 unless (looks_like_number($values[1]));
    return 0 unless (looks_like_number($values[2]));

    return (($values[0] * 60*60 + $values[1] * 60 + $values[2] ) * 1000 + $mstime);
}


sub tags::lapdcsvheader {
    return sprintf("%s\n",
        '"datestamp","venue","boxid","eventid","rx","tagid","batt","rssi","finishms","startms","groupms","lapms","lapnumber","groupnumber","gapms","skippedflag","dbid","lastname","firstname"');
}

sub tags::rawdcsvheader {
    return sprintf("%s\n",
        '"datestamp","venue","antenna","rx","tagid","rssi","finishms","startms","lapms","record","reason","dbid","lastname","firstname"');
}

sub tags::infocsvheader {
    return sprintf("%s\n",
        '"datestamp","venue","boxid","eventid","rx","tagid","batt","rssi","finishms","startms","groupms","lapms","lapnumber","groupnumber","gapms","skippedflag","dbid","lastname","firstname"');
}

my $CurrentFileDate = "";
my $FoutFilename = "";
my $RoutFilename = "";


sub lapdheader {
    if ($FoutFilename ne "") {
        printf STDERR "lapheader\n";
        print FOUT lapdcsvheader();
        flush FOUT;
    }
}

sub openfout {

    my ($venuename, $servermode) = @_;

    my $epoch = Time::HiRes::gettimeofday();

    my $GMTOffset = gmoffset::getgmtoffset();
    my $dt = DateTime->from_epoch(epoch => $epoch + $GMTOffset);
    my $datestamp = $dt->strftime("%Y%m%d", $epoch + $GMTOffset);

    return 0 if ($datestamp eq $CurrentFileDate);
    $CurrentFileDate = $datestamp;

    if ($FoutFilename ne "") {
        printf STDERR "CLOSING %s\n", $FoutFilename;
        close FOUT;
        close ROUT;
        #close SOUT;
        $FoutFilename = "";
        $RoutFilename = "";
    }

    my $lapdfilename = sprintf("lapd/lapd-%s-%s.csv", $venuename, $datestamp);
    my $rawdfilename = sprintf("rawd/rawd-%s-%s.csv", $venuename, $datestamp);
    $FoutFilename = $lapdfilename;

    if (-e $lapdfilename && -s $lapdfilename) {
        printf STDERR "APPEND %s\n", ">>". $lapdfilename;
        open (FOUT, ">>". $lapdfilename) || die "Cannot create ">>".$lapdfilename\n";
        open (ROUT, ">>". $rawdfilename) || die "Cannot create ">>".$rawdfilename\n";
        return 1;
    }

    printf STDERR "NEW %s\n", ">".$lapdfilename;
    open (FOUT, ">". $lapdfilename) || die "Cannot create ">". $lapdfilename\n";
    print FOUT lapdcsvheader();

    printf STDERR "NEW %s\n", ">".$rawdfilename;
    open (ROUT, ">". $rawdfilename) || die "Cannot create ">". $rawdfilename\n";
    print ROUT rawdcsvheader();

    #my $infofilename = sprintf("lapd-%s-%s.csv", $venuename, $datestamp);
    #$FoutFilename = $infofilename;

    #if (-e $infofilename && -s $infofilename) {
    #    printf STDERR "APPEND %s BBB\n", $infofilename;
    #    $infofilename = sprintf(">>lapd-%s-%s.csv", $venuename, $datestamp);
    #    open (IOUT, $infofilename) || die "Cannot create $infofilename\n";
    #    return 1;
    #}

    #printf STDERR "NEW %s\n", $infofilename;
    #$infofilename = sprintf(">lapd-%s-%s.csv", $venuename, $datestamp);
    #open (IOUT, $infofilename) || die "Cannot create $infofilename\n";

    #print FOUT infocsvheader();

    if ($servermode) {
        my $loginfo = sprintf("File: %s", $lapdfilename);
        syslog('warning', $loginfo);
    }
    return 1;
}
# ################################################################################################################### #

# organizer,venue,   description,        distance,minspeed,maxspeed,gaptime,timezone,activeflag
# BVC,      BVCTrack,"Burnaby Velodrome",0.2,     25,      70,      2,      PST8PDT, 1

my $Distance = 0.138;
my $MinSpeed = 25;
my $MaxSpeed = 72;
my $GapTime = 1000; # mS
my $VenueName = "FCVTrack";

#my $RxID = 0;

my $MinTime = dist2ms($Distance, $MaxSpeed);
my $MaxTime = dist2ms($Distance, $MinSpeed);

sub dist2ms {
    my ($distance, $speed) = @_;
    return ($distance / $speed) *60 * 60 * 1000;
}

sub ms {
    my ($ms) = @_;
    return sprintf("%.4f", $ms);
}

# ################################################################################################################### #

my %LastLapTime;
my %LastSeen;
my %LastValid;
my %LapNumbers;
my %Skipped;
my $GroupStartMS = 0;
my $GroupLastMS = 0;
my $GroupNumber = 0;

my %DBLast;
my %DBID;
my %DBLastName;
my %DBFirstName;

my %MissingLaps;
my %CountedLaps;
my %TotalMissingLaps;
my %TotalCountedLaps;

my $CurrentEvent = "";

# print a cell in double quotes, followed by a comma or carriage return for the last cell on
# a line.
#
sub print_csv_str {
    my ($cell, $last) = @_;

    my $cellstr = sprintf("\"%s\"", $cell);
    if ($last) { 
        $cellstr .= sprintf("\n"); 
    }
    else { 
        $cellstr .= sprintf(","); 
    }

    return $cellstr;
}

my $count = 0;

sub printmsg {
    my ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, $mesg) = @_;

    printf STDERR "[%3d][%2d:%d:%3d] %s %s: FinishMS:%4.1f - StartMS:%4.1f  = LapMS: %4.1f (%4.1f) %4.1f %s\n",
           $count, $lapnumber, $antenna, $rssi,
           $timestamp, $tagid, 

           $MS/1000, 
           $lastValidMS/1000, 
           $lapValidMS/1000, 
           $lapSeenMS/1000, 
           $lastLapTime/1000,
           $mesg;
}


sub tags::record_tag {

    my ($dbsql, $rxid, $tagid, $timestamp, $epoch, $antenna, $rssi, $readermode, $trackmode, $servermode) = @_;

    $count++;

    printf STDERR "\nprocess: %s %s finishms: %s antenna: %d rssi: %d\n", $tagid, $timestamp, $epoch, $antenna, $rssi;



    my $skippedflag = 0;

    my $gamps = 0;

    if (openfout($VenueName, $servermode)) {
        undef %LastLapTime;
        undef %LastSeen;
        undef %LastValid;
        undef %LapNumbers;
        undef %Skipped;
        $GroupStartMS = 0;
    }

    #
    # get user name from tagid
    #
    my $dbid = "";
    my $dbLastName = "";
    my $dbFirstName = "";
    my $sql = "SELECT L.id, L.last_name, L.first_name FROM core_licenseholder L WHERE L.existing_tag = ?;";
               #select l.id, l.last_name, l.first_name from core_licenseholder l where l.existing_tag = '8F7200041014';

    my $sth = $dbsql->prepare($sql) || die "Cannot prepare " . $sql;

    $sth->execute($tagid) || die "Cannot execute " . $sth->errstr;

    if (my $row = $sth->fetchrow_hashref()) {
        #printf STDERR Dumper($row);
        $dbid = $row->{'id'};
        $dbFirstName = $row->{'first_name'};
        $dbLastName = $row->{'last_name'};

        #printf STDERR "record_tag: id: %s last: %s first: %s\n", $dbid, $dbLastName, $dbFirstName;
    }
    else {
        #printf STDERR "record_tag: cannot find tag info for %s\n", $tagid;
    }
    $sth->finish();

    # compute the elapsed time in milliseconds
    #
    my $MS = $epoch * 1000;

    my $lastSeenMS = 0; # previously seen MS
    my $lastLapTime = 0;# previously seen MS
    my $lastValidMS = 0;# previously seen MS
    my $lapSeenMS = 0;  # laptime MS
    my $lapValidMS = 0; # laptime MS
    my $groupMS = 0;    # time from beginning of this group
    my $gapms = 0;

    my $recordflag = 1;
    my $clearflag = 0;
    my $failstr = "";
    
    my $skipped = 0;
    my $lapnumber = 0;

    #
    # check if first time we have seen this rider for this workout
    #
    unless (defined $LastValid{$tagid}) {
        printmsg (0, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, 0, 0, 0, "FIRST Start");
        $lastValidMS = 0;
        $lapValidMS = 0;
        $LastLapTime{$tagid} = 0;
        $LastSeen{$tagid} = $MS;
        $LastValid{$tagid} = $MS;
        $LapNumbers{$tagid} = 1;
        $Skipped{$tagid} = 0;
        $recordflag = 1;
        $clearflag = 0;
        $failstr = "First";
    }
    else {

        $lapnumber = $LapNumbers{$tagid};

        $lastLapTime = $LastLapTime{$tagid};

        $lastSeenMS =  $LastSeen{$tagid};
        $lastValidMS =  $LastValid{$tagid};

        $lapSeenMS = $MS - $lastSeenMS;
        $lapValidMS = $MS - $lastValidMS;
        $LastSeen{$tagid} = $MS;

        $skipped = $Skipped{$tagid};

        #$LastValid{$tagid} = $MS;


        #$CountedLaps{join("-", $tagid, $filedate)}++;
        $CountedLaps{$tagid}++;
        #$TotalCountedLaps{$filedate}++;


        # XXX MinTime needs some work for track, it is possible to get a valid lap from two swipes to test
        # XXX if they are too close together, resulting in a very high speed lap equivalent, e.g. 65kph
        # see lapd-BVCTrack Oct 26, t025EB2 10.92 at about 19:55
        #
        #

        unless ($lapnumber > 1) {

            #printf STDERR "\n[%d]FIRST LAP\n", $lapnumber;

            # check for second read
            #
            if ($lapSeenMS < 2000) {
                #printf STDERR "\n%s %s: MS: %4.1f %4.1f FIRST Lap SECOND READ\n", $timestamp, $tagid, $MS;
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "FIRST second read");
                $recordflag = 0;
                $clearflag = 1;
                $failstr = "First SecondRead";
            }
            elsif ($lapSeenMS < ($SCALE * 8000)) {
                printf STDERR "\n%s %s: MS: %4.1f %4.1f FIRST Lap FAR READ (%d)\n", $timestamp, $tagid, $MS, ($SCALE * 8000);
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "FIRST far read");
                $recordflag = 0;
                $clearflag = 1;
                $failstr = "First FarRead";
            }

            # need to be going > 20kmh
            #
            elsif ($lapValidMS > ($SCALE * 36000)) {
                printf STDERR "\n%s %s: MS: %4.1f %4.1f FIRST Lap TOO SLOW < 20kmh (%d)\n", $timestamp, $tagid, $MS, ($SCALE * 36000);
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "FIRST Lap too slow < 20kmh");
                $recordflag = 0;
                $clearflag = 0;
                $failstr = "First TooSlow";
            }

            # need to be going < 40
            #
            elsif ($lapValidMS < ($SCALE * 18000)) {
                printf STDERR "\n%s %s: MS: %4.1f %4.1f FIRST Lap TOO FAST > 40kmh (%d)\n", $timestamp, $tagid, $MS, ($SCALE * 18000);
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "FIRST Lap too fast > 40kmh");
                $recordflag = 0;
                $clearflag = 0;
                $failstr = "First TooFast";
            }

            # not too fast or too slow
            #
            else {
                printf STDERR "\n\n********************************\n\n\n";
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "FIRST OK");
                $LapNumbers{$tagid}++;
                $Skipped{$tagid} = 0;
                $LastLapTime{$tagid} = $lapValidMS;
                $recordflag = 0;
                $clearflag = 0;
                $failstr = "First OK";
                printf STDERR "\n";
            }

        }
        else {

            #printf STDERR "\n[%d]Workout Started\n", $lapnumber;

            # check for second read
            #
            if ($lapSeenMS < 2000) {
                #printf STDERR "\n%s %s: MS: %4.1f %4.1f FIRST Lap TOO SLOW < 20kmh\n", $timestamp, $tagid, $MS;
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Workout second read");
                $recordflag = 0;
                $clearflag = 0;
                $failstr = "Second Read";
            }

            elsif ($lapSeenMS < ($SCALE * 9000)) {
                #printf STDERR "\n%s %s: MS: %4.1f %4.1f FIRST Lap TOO SLOW < 20kmh\n", $timestamp, $tagid, $MS;
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Workout far read");
                $recordflag = 0;
                $clearflag = 0;
                $failstr = "Far Read";
            }

            # rider had left the track if 4 x MaxTime has elapsed since last record
            #
            elsif ($lapValidMS > (4*$MaxTime)) {
                printf STDERR "\n\n********************************\n\n\n";
                $lastValidMS = 0;
                $lapValidMS = 0;
                $clearflag = 1;
                $recordflag = 0;
                $failstr = "Left Track";
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Left track");
            }

            # check for to fast
            #
            # if > 72, 
            elsif ($lapValidMS < ($SCALE * 10000)) {

                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Too Fast > 72kmh");

                $lastValidMS = 0;
                $lapValidMS = 0;
                $recordflag = 0;
                $failstr = ">72";
            }
            # if > 60, then previous lap > 40
            elsif (($lapValidMS < ($SCALE * 12000 )&& $lastLapTime > 0 && $lastLapTime > ($SCALE * 18000))) {
                
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Too Fast > 60kmh previous < 40kmh");
                $recordflag = 0;
                $failstr = ">60&<40";
            }
            # if > 50, then previous lap > 35
            elsif (!$skipped && ($lapValidMS < ($SCALE * 14400 )&& $lastLapTime > ($SCALE * 20570))) {
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Too Fast > 50 previous < 35");
                $recordflag = 0;
                $failstr = ">50&<35";
            }
            # if > 40, then previous lap > 25
            elsif (!$skipped && ($lapValidMS < ($SCALE * 18000 )&& $lastLapTime > ($SCALE * 28800)) && $LapNumbers{$tagid} > 5) {
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Too Fast > 40 previous < 25");
                $recordflag = 0;
                $failstr = ">40&<25";
            }


            # we may have missed a tag read if time is between MaxTime and 4 x MaxTime 
            #
            elsif ($lapValidMS > ($SCALE * 30000 )&& $LapNumbers{$tagid} > 3) {

                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "Possible Skipped track");
                printf STDERR "\n";

                #$MissingLaps{join("-", $tagid, $filedate)}++;
                $MissingLaps{$tagid}++;
                #$TotalMissingLaps{$filedate}++;
                $skippedflag++;
                $LapNumbers{$tagid}++;
                $LastLapTime{$tagid} = $lapValidMS / 2;
                $Skipped{$tagid} = 1;
                $failstr = "OK Skipped";
            }

            # check for to fast
            #
            #elsif ($lapSeenMS < $MinTime) {
            #    ###printf STDERR "%s %s: MS: %4.1f - %4.1f  = %4.1f TOO FAST < %4.1f TRACK\n",
            #    ###       $timestamp, $tagid, $MS/1000, $lastSeenMS/1000, $lapSeenMS/1000, $MinTime/1000 if ($trackmode);;
            #    #$lastValidMS = 0;
            #    #$lapValidMS = 0;
            #    #$LapNumbers{$tagid} = 0;
            #    $LapNumbers{$tagid}--;
            #    $recordflag = 0;
            #    #return "";
            #}
            else {
                printmsg ($lapnumber, $antenna, $rssi, $timestamp, $tagid, $MS, $lastValidMS, $lapValidMS, $lapSeenMS, $lastLapTime, "OK");
                printf STDERR "\n";
                $LapNumbers{$tagid}++;
                $Skipped{$tagid} = 0;
                $LastLapTime{$tagid} = $lapValidMS;
                $failstr = "OK";
            }
        }
        
    }


    # 
    # dump the data fields to the log file
    # that file will be forwarded to the database server
    #
    my $rcsvline = "";

    $rcsvline .= print_csv_str($timestamp, 0);               # 0
    $rcsvline .= print_csv_str($VenueName, 0);               # 1
    $rcsvline .= print_csv_str($antenna, 0);                 # 2 BOX
    $rcsvline .= print_csv_str($rxid);                       # 3 RX
    $rcsvline .= print_csv_str($tagid, 0);                   # 4 tag id
    $rcsvline .= print_csv_str("$rssi", 0);                  # 5 RSSI
    $rcsvline .= print_csv_str(ms($MS), 0);                  # 6 time lap finished
    $rcsvline .= print_csv_str(ms($lastValidMS), 0);         # 7 time lap started (if available)
    $rcsvline .= print_csv_str(ms($lapValidMS), 0);          # 8 lap time (if start time available)
    $rcsvline .= print_csv_str($recordflag, 0);              # 9 recorded flag
    $rcsvline .= print_csv_str($failstr, 0);                 # 10 recorded flag
    $rcsvline .= print_csv_str($dbid, 0);                    # 11 user database ID
    $rcsvline .= print_csv_str($dbLastName, 0);              # 12 user Last Name
    $rcsvline .= print_csv_str($dbFirstName, 1);             # 13 user First Name


    print ROUT $rcsvline;
    flush ROUT;

    if ($clearflag) {
        $Skipped{$tagid} = 0;
        $LastLapTime{$tagid} = 0;
        $LastSeen{$tagid} = $MS;
        $LastValid{$tagid} = $MS;
        $LapNumbers{$tagid} = 0;
        printf STDERR "################################\n";
    }
    unless($recordflag) {
            return;
    }

    $LastValid{$tagid} = $MS;
    $lapnumber = $LapNumbers{$tagid};

    # Get the elapsed time from the beginning of this group
    #
    # If there is no current group start time then use this as the start of one
    #
    unless ($GroupStartMS) {
        printf STDERR "%s: GroupStartMS: ZERO %d\n", $tagid, $groupMS if ($DEBUG);
        $GroupStartMS = $MS;
        $GroupLastMS = $MS;
        $groupMS = 0;
        $GroupNumber = 0;
    }
    # 
    # if the time from the last recorded time in the group exceeds the GapTime then reset
    #
    else {
        $gapms = $MS - $GroupLastMS;

        if ($gapms > $GapTime) {
            if ($trackmode) {
                printf STDERR "%s: GroupStartMS: %d %d too large > %d\n", $tagid, $GroupStartMS, $MS, $GapTime if ($DEBUG);
            }

            if ($gapms > $MaxTime) {
                $gapms = 0;
            }
            $GroupStartMS = $MS;
            $GroupLastMS = $MS;
            $groupMS = 0;
            $GroupNumber = 0;
        }
        #
        # within the GapTime so compute the groupMS and save the last group time
        #
        else {
            $groupMS = $MS - $GroupStartMS;

            # ensure that groupMS is non-zero
            $groupMS++ unless($groupMS);
            $GroupLastMS = $MS;
            $GroupNumber++;
            $gapms = 0;
            printf STDERR "%s: GroupStartMS: %d %d < gap: %d\n", $tagid, $GroupStartMS, $MS, $groupMS if ($DEBUG);
        }
    }

    # 
    # dump the data fields to the log file
    # that file will be forwarded to the database server
    #
    my $csvline = "";

    $csvline .= print_csv_str($timestamp, 0);               # 0
    $csvline .= print_csv_str($VenueName, 0);               # 1
    $csvline .= print_csv_str("0", 0);                      # 2 box id
    $csvline .= print_csv_str($antenna, 0);                 # 3 BOX
    $csvline .= print_csv_str($rxid);                       # 4 RX
    $csvline .= print_csv_str($tagid, 0);                   # 5 tag id
    $csvline .= print_csv_str("1", 0);                      # 6 BATT
    $csvline .= print_csv_str("$rssi", 0);                  # 7 RSSI
    $csvline .= print_csv_str(ms($MS), 0);                  # 8 time lap finished
    $csvline .= print_csv_str(ms($lastValidMS), 0);         # 9 time lap started (if available)
    $csvline .= print_csv_str(ms($groupMS), 0);             # 10group time (from beginning of group lap)
    $csvline .= print_csv_str(ms($lapValidMS), 0);          # 11 lap time (if start time available)
    $csvline .= print_csv_str($lapnumber, 0);               # 12 Lap number for consecutive laps 
    $csvline .= print_csv_str($GroupNumber, 0);             # 13 Group number for group entry
    $csvline .= print_csv_str(ms($gapms), 0);               # 14 Group number for group entry
    $csvline .= print_csv_str($skippedflag, 0);             # 15 Skipped Lap flag

    $csvline .= print_csv_str($dbid, 0);                    # 16 user database ID
    $csvline .= print_csv_str($dbLastName, 0);              # 17 user Last Name
    $csvline .= print_csv_str($dbFirstName, 1);             # 18 user First Name


    print FOUT $csvline;
    flush FOUT;

    #
    # return the csvline so that clients connected via
    # tcp can also get a copy
    #
    return $csvline;
}

1;
