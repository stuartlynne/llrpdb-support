


package service;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK @EXPORT_TAGS);

$VERSION        = 1.00;
@ISA            = qw(Exporter);
@EXPORT         = qw(do_process do_service sighandler);

use Text::CSV;
use Socket qw(IPPROTO_TCP SOL_SOCKET SO_LINGER SO_KEEPALIVE inet_aton sockaddr_in TCP_NODELAY);
use Socket::Linux qw(TCP_NODELAY TCP_KEEPIDLE TCP_KEEPINTVL TCP_KEEPCNT);
use Sys::Syslog qw (:standard :macros);
use Data::Dumper;

use lib::tags qw(record_tag lapdcsvheader infocsvheader);

use lib::reader qw(
        reader_setup 
        rospec_setup 
        rospec_start 
        rospec_started 
        get_antenna_config 
        get_antenna_ids
        process_reader_responses
        getlasttagread
        reader_shutdown        
        );


#my $database_file = "RaceDB.sqlite3";
#my $CSV = Text::CSV->new({ binary=> 1, eol => $/ });
#my $dbsql = DBI->connect("dbi:SQLite:dbname=$database_file","","",{'PrintError' => 0});
#die "Cannot open database: " . $database_file . "\n" unless ($dbsql);



# clientsockets -hash indexed by client file handle, contains data to send to the client
# clientports -  hash indexed by client file handle, contains the sockport for the client, determines antenna client is interested in.

my (%clientsockets, %clientports);

my $ExitFlag = 0;
my $RestartFlag = 0;
my $InterruptedFlag = 0;


sub service::sighandler {
    my ($sig) = @_;
    printf STDERR "Caught signal: %s\n", $sig;

    if ($sig eq "INT") {
        printf STDERR "Interrupted\n";
        $InterruptedFlag = 1;
        return;
    }
    if ($sig eq "HUP") {
        printf STDERR "Restarting\n";
        $RestartFlag = 1;
        return;
    }
    if ($sig eq "QUIT") {
        printf STDERR "Quit\n";
        $ExitFlag = 1;
        return;
    }
}


sub service::do_process {

    my ($dbsql, $rxid, $tagid, $timestamp, $epoch, $antenna, $rssi, $readermode, $trackmode, $servermode) = @_;

    my $csvline = tags::record_tag($dbsql, $rxid, $tagid, $timestamp, $epoch, $antenna, $rssi, $readermode, $trackmode, $servermode);
    
    if ($readermode eq "Immediate") {
        $antenna = 5550;
    }
    elsif ($readermode eq "One-Shot") {
        $antenna += 5550;
    }

    foreach my $fh (keys %clientports) {
        if ($clientports{$fh} == $antenna) {
            $clientsockets{$fh} .= $csvline;
        }
        elsif ($readermode eq "One-Shot") {
            $clientsockets{$fh} .= "\n";
        }
    }
    return $csvline;

}

# listensocket
# create a listen socket on a specific port
#
sub listensocket {

    my ($port) = @_;

    # open the listen socket
    #
    my $listensock = IO::Socket::INET->new(Listen => 1, Reuse => 1, MultiHomed => 1, LocalPort => $port);

    return unless (defined($listensock));

    printf STDERR "Listen Socket Setup port: %d\n", $port;
    setsockopt($listensock, SOL_SOCKET, SO_LINGER, pack("l*",0,0));
    setsockopt($listensock, SOL_SOCKET, SO_LINGER, 0);
    setsockopt($listensock, IPPROTO_TCP, TCP_KEEPIDLE, 10);
    setsockopt($listensock, IPPROTO_TCP, TCP_KEEPINTVL, 5);
    setsockopt($listensock, IPPROTO_TCP, TCP_KEEPCNT, 5);
    return $listensock;
}

sub clientcleanup {
    my ($fh, $msg) = @_;
    # client socket closed
    #printf STDERR "Client closed connection\n";
    #$sel->remove($fh);
    close($fh);
    delete $clientsockets{$fh};
    foreach my $cfh (keys %clientports) {
        next unless ($fh eq $cfh);
        delete $clientports{$cfh};
        last;
    }
    my $count1 = scalar(keys %clientsockets);
    my $count2 = scalar(keys %clientports);
    printf STDERR "Closed Client Socket: %d:%d %s\n", $count1, $count2, $msg;
}

###################################################################################################################### #
#
# Server Mode - Repeat forever....
#
# 1. Open a socket to connect to server.
# 2. Set socket options to enable TCP KEEPALIVE
# 3. Process until socket closes.
# 4. Sleep
# 5. Repeat
#


sub service::do_service {

    my ($readername, $readermode, $servermode, $dbsql, $RFTransmitPowerMax, $RFReceiverSensitivity) = @_;


    my $lowdutymode = 0;
    my $lowdutyflag = 0;
    my $lowdutytimeout = 5*60;
    #my $lowdutytimeout = 5;
    my $trackmode = 0;
    my $readersock;

    if ($readermode eq "Low-Duty") {
        $readermode = "Immediate";
        $lowdutymode = 1;
        $lowdutyflag = 1;
        $trackmode = 1;
    }


    # Use an eval so that we can use die in signal handler to close socket in pending read
    # and continue operation.
    #
    my @listensockets;
    my $readersocket;
    eval {

        # setup sighandlers so we can restart, interrupt and/or exit
        #
        $SIG{ 'INT' } = 'service::sighandler';
        $SIG{ 'HUP' } = 'service::sighandler';
        $SIG{ 'QUIT' } = 'service::sighandler';
        
        # create select and add the reader and listen sockets
        #
        my $sel = IO::Select->new();


        # Open the reader and client on port 5550, reader_setup may die so use an eval
        # XXX May want to have a retry timeout
        # 
        eval {
            $readersock = reader::reader_setup($readername, $readermode, $RFTransmitPowerMax, $RFReceiverSensitivity);

            die "Cannot connect to readr\n" unless (defined($readersock));
            #printf STDERR "readersock: %s\n", Dumper($readersock);

            syslog('warning', sprintf("Connected to %s", $readername)) if ($servermode);

            $sel->add($readersock);

            reader::rospec_setup($readersock, $dbsql, $readername, $readermode, $lowdutyflag, $servermode);

            if (($readermode eq "Immediate")) {
                my $lsock = listensocket(5550);
                if (defined($lsock)) {
                    $listensockets[5550] = $lsock;
                    $sel->add($lsock);
                }
                $lsock = listensocket(5555);
                if (defined($lsock)) {
                    $listensockets[5555] = $lsock;
                    $sel->add($lsock);
                }
            }
            if ($readermode eq "One-Shot") {
                my $lsock;
                for (my $port = 5551; $port <= 5554; $port++) {
                    $lsock = listensocket($port);
                    if (defined($lsock)) {
                        $listensockets[$port] = $lsock;
                        $sel->add($lsock);
                    }
                }
                $lsock = listensocket(5556);
                if (defined($lsock)) {
                    $listensockets[5556] = $lsock;
                    $sel->add($lsock);
                }
            }
        };
        warn $@ if $@;


        # Loop until program interrupted
        #
        # XXX 
        # Need to retry reader_connect if either fails
        #
        OUTER:
        until ($ExitFlag) {

            #printf STDERR "Start\n";

            $InterruptedFlag = 0;

            # get reader ready sockets
            #
            if (my @readyread = $sel->can_read(20)) {
                #printf STDERR "Reader Ready\n";
                IOLOOP:
                foreach my $fh (@readyread) {

                    # process data on the reader socket
                    if (defined($readersock) && ($fh == $readersock)) {
                        reader::rospec_started();
                        #print STDERR "<";

                        reader::process_reader_responses($readersock, $dbsql, $readermode, $trackmode, $servermode);
                        my $currenttime = Time::HiRes::gettimeofday();

                        my $elapsed = $currenttime - reader::getlasttagread();
                        #printf STDERR "Check %d %d %d %d\n", $lowdutymode, $lowdutyflag, $lowdutytimeout, $elapsed; 

                        if ($lowdutymode) {
                            if (!$lowdutyflag && ($elapsed > $lowdutytimeout)) {
                                $lowdutyflag = 1;
                                reader::rospec_setup($readersock, $dbsql, $readername, $readermode, $lowdutyflag);
                            }
                            elsif ($lowdutyflag && ($elapsed < $lowdutytimeout)) {
                                $lowdutyflag = 0;
                                reader::rospec_setup($readersock, $dbsql, $readername, $readermode, $lowdutyflag);
                            }
                        }
                        if ($readermode eq "One-Shot") {
                            foreach $fh (keys %clientsockets) {
                                $clientsockets{$fh} .= "\n";
                            }
                        }


                        #print STDERR ">";
                        next IOLOOP;
                    }

                    # check for and process data on the listen socket
                    for (my $port = 5550; $port <= 5556; $port++) {
                        next unless (defined($listensockets[$port]) && $listensockets[$port]);
                        next unless ($fh == $listensockets[$port]);

                        my $new = $listensockets[$port]->accept();
                        printf "Accepting Client Connection from %s (%d)", $new->peerhost(), $new->sockport();
                        syslog('warning', sprintf("Accepting Client Connection from %s (%d)", $new->peerhost(), $new->sockport())) if ($servermode);

                        if (($port == 5555) || ($port == 5556)) {

                            $new->send(sprintf("ReaderMode, ProgramName, ReaderName, DutyCycle, Antennas\n", $0, $readername));
                            if ($readermode eq "Immediate") {
                                $new->send(sprintf("Imediate, %s, %s, %s, %s\n", $0, $readername, $lowdutyflag ? "Low-Duty" : "Full-Duty", reader::get_antenna_ids()));
                            }
                            if ($readermode eq "One-Shot") {
                                $new->send(sprintf("One-Shot, %s, %s, %s\n", $0, $readername, "On-Demand", reader::get_antenna_ids()));
                            }
                            close($new);
                            next IOLOOP;
                        }
                        
                        $sel->add($new);
                        $clientsockets{$new} = "";
                        $clientports{$new} = $port;
                        setsockopt($new, SOL_SOCKET, SO_LINGER, pack("l*",0,0));

                        if ($port == 5550) {
                            $new->send(tags::lapdcsvheader());
                        }
                        elsif (($port >= 5551) && ($port <= 5554)) {
                            $new->send(tags::lapdcsvheader());
                            reader::rospec_start($readersock, "Normal") if ($readermode eq "One-Shot");
                        }
                        
                        next IOLOOP;
                    }

                    # client socket
                    my $buf = <$fh>;
                    if ($buf) {
                        printf STDERR "Client sent data\n";
                        next;
                    }
                    
                    # client socket closed
                    printf STDERR "Client closed connection\n";
                    $sel->remove($fh);

                    clientcleanup($fh, "Far End Closed");
                }

                #printf STDERR "Check antenna config\n";
                my $NewAntennaIDs = reader::get_antenna_config($readersock, 0);
                #printf STDERR "AntennaIDs: was: %s now %s\n", reader::get_antenna_ids(), $NewAntennaIDs;
                if ($lowdutyflag) {
                    if (reader::get_antenna_ids() ne $NewAntennaIDs) {
                        printf STDERR "AntennaIDs: was: %s now %s\n", reader::get_antenna_ids(), $NewAntennaIDs;
                        syslog('warning', "Antennas Changed was: %s, now: %s", reader::get_antenna_ids(), $NewAntennaIDs);
                        last OUTER;
                    }
                }
            }
            else {
                printf STDERR "Reader Timeout\n";
                printf STDERR ".";
                last OUTER if ($RestartFlag);
                last OUTER if ($ExitFlag);
                last OUTER if ($readermode eq "Immediate");
                if ($InterruptedFlag) {
                    if ($readermode eq "One-Shot") {
                        reader::rospec_start($readersock, "Interrupted");
                    }
                    $InterruptedFlag = 0;
                }
                #printf STDERR "BBB\n";
                if ($readermode eq "One-Shot") {
                    my $count1 = scalar(keys %clientsockets);
                    reader::rospec_start($readersock, "Timed out") if ($count1);
                }

                #printf STDERR "CCC\n";
                my $CurrentTime = Time::HiRes::gettimeofday();
                #printf STDERR "Time since last tag: %d\n", $CurrentTime - reader::getlasttagread(); 


                #printf STDERR "Check antenna config\n";
                my $NewAntennaIDs = reader::get_antenna_config($readersock, 0);
                if (reader::get_antenna_ids() ne $NewAntennaIDs) {
                    printf STDERR "AntennaIDs: was: %s now %s\n", reader::get_antenna_ids(), $NewAntennaIDs;
                    syslog("Antennas Changed was: %s, now: %s", reader::get_antenna_ids(), $NewAntennaIDs);
                    last OUTER;
                }

            }
            printf STDERR ".";
            #printf STDERR "Reader Ready Finished\n";

            # look for any exceptions, 
            #
            foreach my $fh (my @readyexception = $sel->has_exception(0)) {
                printf "Client exception\n";
                $sel->remove($fh);
                clientcleanup($fh, "Exception");
            }

            # look for any client sockets we can write to
            #
            foreach my $fh (my @readywrite = $sel->can_write(0)) {
                #printf STDERR "Client write\n";
                next unless (defined($clientsockets{$fh}) && length($clientsockets{$fh}));
                $fh->send($clientsockets{$fh});
                $clientsockets{$fh} = "";
                next unless ($readermode eq "One-Shot");

                # auto-close one-shot connections after sending tag report
                #
                $sel->remove($fh);
                clientcleanup($fh, "One-Shot Auto-Close");
            }
        }
    };
    warn $@ if ($@);
    
    printf STDERR "Eval exit\n";
    #printf STDERR "readersock: %s\n", Dumper($readersock);

    # if the reader connection is open disable the reader ROSpec and close the connection
    #
    if (defined($readersock)) {
        syslog('warning', sprintf("Closing Reader Socket %s", $readername)) if ($servermode);
        reader::reader_shutdown($readersock);
        undef $readersock;
    }
    else {
        printf STDERR "Reader: %s was not connected\n", $readername;
    }

    # if there are any client connections close them
    #
    foreach my $fh (keys %clientsockets) {
        #setsockopt($fh, SOL_SOCKET, SO_LINGER, 0);
        clientcleanup($fh, "General Close");
    }

    # close the listening socket
    #
    for (my $port = 5550; $port <= 5554; $port++) {
        next unless (defined($listensockets[$port]) && $listensockets[$port]);
        printf STDERR "Disconnecting Listen Socket\n";
        syslog('warning', sprintf("Clossing Listen Connection")) if ($servermode);
        close($listensockets[$port]);
        delete $listensockets[$port];
    }

    # exit if quit signal was received
    #
    exit(0) if ($ExitFlag);

    printf STDERR "Finished\n";

    sleep(5);
}



1;
