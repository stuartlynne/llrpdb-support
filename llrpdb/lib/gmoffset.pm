

package gmoffset;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK @EXPORT_TAGS);

use Time::Local;
use DateTime::TimeZone;
use DateTime::Locale;
use DateTime::Format::Strptime;
use Data::Dumper;

$VERSION        = 1.00;
@ISA            = qw(Exporter);
@EXPORT         = qw(getlocaloffset getgmtoffset getepoch);

my $GMTOffset = gmoffset::getlocaloffset();

my $ltz = "UTC";
my $date_format = new DateTime::Format::Strptime( pattern => '%F %T', time_zone => $ltz,);

sub gmoffset::getlocaloffset {

    my @t = localtime(time);
    return timegm(@t) - timelocal(@t);
}

sub gmoffset::getgmtoffset {
    return $GMTOffset;
}

sub gmtoffset::getepoch {

    my ($timestamp, $offset) = @_;
    my $microseconds = $timestamp;

    #printf STDERR "getepoch: %s\n", $timestamp;
    ($timestamp,$microseconds) = split(/\./,$timestamp);
    $timestamp =~ s/T/ /;
 
    #printf STDERR "getepoch: %s\n", $timestamp;
    my $dt = $date_format->parse_datetime($timestamp);
    my $ntimestamp = $dt->strftime("%Y-%m-%d %H:%M:%S");
    #printf STDERR "%s %s\n", $ntimestamp, $microseconds;

    my $endtime = Time::HiRes::gettimeofday();


    my $epoch = $dt->epoch() + ($microseconds / 1000000);

    #printf STDERR "getepoch: %s %s\n", $epoch, $epoch + $offset;
    return $epoch + $offset;
}

1;
