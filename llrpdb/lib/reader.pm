

package reader;

use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK @EXPORT_TAGS);

use lib::gmoffset qw(getlocaloffset getgmtoffset getepoch);

$VERSION        = 1.00;
@ISA            = qw(Exporter);
@EXPORT         = qw(
        reader_setup 
        rospec_setup 
        rospec_start 
        get_antenna_config 
        get_antenna_ids 
        rospec_started
        process_reader_responses
        getlasttagread
        reader_shutdown        
        );

use IO::Socket::INET;
use Socket qw(IPPROTO_TCP SOL_SOCKET SO_LINGER SO_KEEPALIVE inet_aton sockaddr_in TCP_NODELAY);
use Socket::Linux qw(TCP_NODELAY TCP_KEEPIDLE TCP_KEEPINTVL TCP_KEEPCNT);

use Sys::Syslog qw (:standard :macros);

use RFID::LLRP::Builder qw(encode_message decode_message);
use RFID::LLRP::Link qw(
        reader_connect
        reader_disconnect
        read_message
        ),
    transact => {QualifyCore => 0},
    monitor => {QualifyCore => 0};

use RFID::LLRP::Helper qw(delete_rospecs are_identical delete_access_specs
        factory_default cleanup_all compare_subtrees);

use lib::service qw(do_process );
use lib::tags qw(lapdheader);


my $RxID = 0;

#%DELIM = ('DELIMITERS' => ['[[_', '_]]']);
#sub expand {
#    my $str = shift;
#    my $result = Text::Template::fill_in_string ($str, HASH => {@_}, %DELIM);
#}
#sub fexpand {
#    my $fname = shift;
#    my $result = Text::Template::fill_in_file ($fname, HASH => {@_}, %DELIM);
#    $result =~ s/\n\s+\n/\n/sg;
#    return $result;
#}
#sub get_msg_name {
#    return $_[0]->getDocumentElement->nodeName;
#}

#sub max {
#    scalar (@_) || die "No list provided";
#    my $best = shift;
#    foreach (@_) {
#        $best = $_ unless $best >= $_;
#    }
#    return $best;
#}

#sub min {
#    scalar (@_) || die "No list provided";
#    my $best = shift;
#    foreach (@_) {
#        $best = $_ unless $best <= $_;
#    }
#    return $best;
#}

#sub get_values {
#    my ($doc, $xpath) = @_;
#    $xpath .= '/node()';
#    return map { $_->getData } ($doc->findnodes ($xpath));
#}

#sub uniq {
#    my %seen;
#    foreach (@_) {
#        $seen{$_}++;
#    }
#    return keys %seen;
#}


# ex - expand xml
#
# expand an array of references defining xml structure to a string
#
# N.B. xmlns is appended to the opt string for the top level entry to ensure that we can use the 
# correct XML format to set namesapces.
#
# Each array has three entries:
#       0 - node name
#       1 - node options
#       2 - node values - may be a single value or another array reference
#
#my $xmlns = " xmlns:llrp=\"http://www.llrp.org/ltk/schema/core/encoding/xml/1.0\" xmlns:Impinj=\"http://developer.impinj.com/ltk/schema/encoding/xml/1.18\" ";
my $xmlns = " xmlns:llrp=\"http://www.llrp.org/ltk/schema/core/encoding/xml/1.0\" xmlns:Impinj=\"http://developer.impinj.com/ltk/schema/encoding/xml/1.8\" ";

sub ex {
    my ($level, $xml, $top) = @_;

    my @xa = @{$xml};

    my $tag = $xa[0];
    my $opt = $xa[1];
    my $ref = $xa[2];

    if ($top) {
        $opt .= $xmlns;
    }
    my $blanks = "";
    $blanks =~ s/^(.*)/' ' x ($level * 4) . $1/e;

    if (ref($ref) eq 'ARRAY') {
        my @sa = @{$ref};
        my $count = $#sa;
        my @vals;
        for (my $i = 0; $i <= $count; $i++) { $vals[$i] = ex($level+1, $sa[$i], 0); }

        if ($opt eq "") {
            return sprintf ("%s<%s>\n%s%s</%s>\n", $blanks, $tag, join("", @vals), $blanks, $tag);
        }
        else {
            return sprintf ("%s<%s %s>\n%s%s</%s>\n", $blanks, $tag, $opt, join("", @vals), $blanks, $tag);
        }
    }
    if ($opt eq "") {
        return sprintf( "%s<%s> %s </%s>\n", $blanks, $tag, $ref, $tag);
    }
    else {
        return sprintf( "%s<%s %s> %s </%s>\n", $blanks, $tag, $opt, $ref, $tag);
    }
}

sub timestamp {
    my $epoch = Time::HiRes::gettimeofday();
    my $dt = DateTime->from_epoch(epoch => ($epoch + gmoffset::getgmtoffset()));
    return $dt->strftime("%H:%M:%S.%3N");
}

# dot - do transaction
# 
# Take an array style xml reference, convert to a string and call the LTKPerl transact() function
#

sub dot {
    my ($sock, $ref, $BadNewsOK, $Trace, $Msg) = @_;
    my $xmlstr = '<?xml version="1.0" encoding="UTF-8"?>';

    return transact ($sock, ex(0, $ref, 1), BadNewsOK => $BadNewsOK, Trace => $Trace);

}

my $Reader_Offset = 0;
my @Reader_Offsets;

sub mean {
    return sum(@_)/@_;
}


###################################################################################################################### #

my $MaxNumberOfAntennaSupported = 0;
my @ReceiveSensitivityTable;
my @TransmitPowerTable;
my $ReceiveSensitivityMin = 0;
my $ReceiveSensitivityMax = 0;
my $TransmitPowerMin = 0;
my $TransmitPowerMax = 0;

my @AntennasConnected;
my $AntennaIDs = "";

sub reader::get_antenna_ids{
    return $AntennaIDs;
}

my $ModelName = "";

sub get_cap {

    my ($sock) = @_;

    my $capabilities = dot($sock, ["llrp:GET_READER_CAPABILITIES", "MessageID=\"0\"", [
            ["llrp:RequestedData", "", "All"],
            ["Impinj:ImpinjRequestedData", "", [
                ["Impinj:RequestedData", "", "All_Capabilities"],
            ]],
            ]], 1, 0, "Get Reader Capabilities");

    my @ModelName_nodes = $capabilities->findnodes("//Impinj:ModelName/node()");
    $ModelName = $ModelName_nodes[0]->textContent; 

    # "Impinj Speedway R1000"
    # "Speedway R420"
    printf STDERR "ModelName: %s\n", $ModelName;

    my @maxantenna_nodes = $capabilities->findnodes("//MaxNumberOfAntennaSupported/node()");
    my $MaxNumberOfAntennaSupported = $maxantenna_nodes[0]->textContent; 

    printf STDERR "Maximum number of Antennas: %d\n", $MaxNumberOfAntennaSupported;

    my @sensitivity_i_nodes = $capabilities->findnodes("//ReceiveSensitivityTableEntry/Index/node()");
    my @sensitivity_v_nodes = $capabilities->findnodes("//ReceiveSensitivityTableEntry/ReceiveSensitivityValue/node()");

    my @power_i_nodes = $capabilities->findnodes("//TransmitPowerLevelTableEntry/Index/node()");
    my @power_v_nodes = $capabilities->findnodes("//TransmitPowerLevelTableEntry/TransmitPowerValue/node()");


    for (my $i = 0; $i <= $#sensitivity_i_nodes; $i++) {
        my $id = $sensitivity_i_nodes[$i]->textContent; 
        my $v = $sensitivity_v_nodes[$i]->textContent; 
        $ReceiveSensitivityTable[$id] = $v;

        $ReceiveSensitivityMin = $id if ($ReceiveSensitivityMin == 0);
        $ReceiveSensitivityMax = $id if ($ReceiveSensitivityMax < $id);
        #printf STDERR "RFS[%s] %s\n", $id, $v;
    }
    for (my $i = 0; $i <= $#power_i_nodes; $i++) {
        my $id = $power_i_nodes[$i]->textContent; 
        my $v = $power_v_nodes[$i]->textContent; 
        $TransmitPowerTable[$id] = $v;

        $TransmitPowerMin = $id if ($TransmitPowerMin == 0);
        $TransmitPowerMax = $id if ($TransmitPowerMax < $id);
        #printf STDERR "RFP[%s] %s\n", $id, $v;
    }
    syslog('warning', sprintf("Receive Sensitivity Table Range: %d %d\n", $ReceiveSensitivityMin, $ReceiveSensitivityMax));
    syslog('warning', sprintf("Transmit Power Table Range: %d %d\n", $TransmitPowerMin, $TransmitPowerMax));

}

sub reader::get_antenna_config {

    my ($sock, $verbose) = @_;

    my $config = dot($sock, 
            ["llrp:GET_READER_CONFIG", "MessageID=\"0\"", [
                ["llrp:AntennaID", "", "0"],
                ["llrp:RequestedData", "", "0"],
                ["llrp:GPIPortNum", "", "0"],
                ["llrp:GPOPortNum", "", "0"],
            ]], 
            1, 0, "Get Reader Antenna Config");

    my @connected_nodes = $config->findnodes("//AntennaProperties/AntennaConnected/node()");
    my @id_nodes = $config->findnodes("//AntennaProperties/AntennaID/node()");
    my @rfr_nodes = $config->findnodes("//RFReceiver/ReceiverSensitivity/node()");
    my @rft_nodes = $config->findnodes("//RFTransmitter/TransmitPower/node()");

    my $antennaids = "";

    for (my $i = 0; $i <= $#id_nodes; $i++) {
        my $id = $id_nodes[$i]->textContent; 
        my $connected = $connected_nodes[$i]->textContent; 
        my $sensitivity = $rfr_nodes[$i]->textContent; 
        my $power = $rft_nodes[$i]->textContent; 
        printf STDERR "antenna[%s] %s sensitivity: %s dB (%d) power: %s dBm*100 (%d)\n", $id, $connected, 
               $ReceiveSensitivityTable[$sensitivity], $sensitivity, 
               $TransmitPowerTable[$power], $power if ($verbose);

        $antennaids .= sprintf("%s ", $id) if ($connected eq "true");
    }
    printf STDERR "AntennaIDs: %s\n", $antennaids if ($verbose);
    return $antennaids;

}

sub get_reader_temperature {

    my ($sock, $verbose) = @_;

    printf STDERR "Getting Reader Temperature\n";

    my $config = dot($sock, 
            ["llrp:GET_READER_CONFIG", "MessageID=\"0\"", [
                ["llrp:AntennaID", "", "0"],
                ["llrp:RequestedData", "", "0"],
                ["llrp:GPIPortNum", "", "0"],
                ["llrp:GPOPortNum", "", "0"],
                #["Impinj:ImpinjRequestedData", "", [
                #    ["Impinj:RequestedData", "", "All_Configuration"],
                #]],
                ["Impinj:ImpinjRequestedData", "", [
                    ["Impinj:RequestedData", "", "Impinj_Reader_Temperature"],
                ]],
            ]], 
            1, 0, "Get Reader Temperature");

    my @temperature_nodes = $config->findnodes("//Impinj:Temperature/node()");
    #printf STDERR "Temperature nodes: %d\n", $#temperature_nodes;
    for (my $i = 0; $i <= $#temperature_nodes; $i++) {
        my $temperature = $temperature_nodes[$i]->textContent; 
        printf STDERR "Temperature: %s\n", $temperature;
    }
    #return $antennaids;

}

# reader_setup
# 
sub reader::reader_setup {
    
    my ($readername, $readermode, $TransmitPowerMax, $ReceiverSensitivity) = @_;

    my $trace = 0;

    printf STDERR "Reader Setup\n";
    printf STDERR "%s: Connecting to %s (%s)\n", timestamp(), $readername, $readermode;

    #my ($doc, $buf);
    #($sock, $doc, $buf) = reader_connect ($readername);
    my $sock = reader_connect ($readername);

    printf STDERR "%s: Connected to %s (%s)\n", timestamp(), $readername, $readermode;
    $sock || die "failed to connect";

    #setsockopt($sock, IPPROTO_TCP, TCP_NODELAY, 1);
    setsockopt($sock, SOL_SOCKET, SO_LINGER, 0);
    setsockopt($sock, IPPROTO_TCP, TCP_KEEPIDLE, 10);
    setsockopt($sock, IPPROTO_TCP, TCP_KEEPINTVL, 5);
    setsockopt($sock, IPPROTO_TCP, TCP_KEEPCNT, 5);

    # empty out channel
    #
    cleanup_all ($sock);

    # Initialize reader
    #   reset to factory default, 
    #   get capabilities and config, 
    #   disable and delete all ROSpecs
    #
    dot($sock, ["llrp:SET_READER_CONFIG", "MessageID=\"0\"", [["llrp:ResetToFactoryDefault", "", "true"], ]], 0, 0, "Set Reader to Factory Defaults");
    dot($sock, ["Impinj:IMPINJ_ENABLE_EXTENSIONS", "MessageID=\"0\"", ""], 0, 0, "Enable Impinj Extensions");
    

    get_cap($sock);

    $AntennaIDs = get_antenna_config($sock, 1);
    syslog('warning', "%s Antennas: %s", $ModelName, $AntennaIDs);

    if ($ModelName eq "Speedway R420") {
        printf STDERR "Get Temp\n";
        get_reader_temperature($sock);
    }

    dot($sock, ["llrp:DISABLE_ROSPEC", "MessageID=\"0\"", [["llrp:ROSpecID", "", "0"]]], 1, 0, "Disable All ROSpecs");
    dot($sock, ["llrp:DELETE_ROSPEC", "MessageID=\"0\"", [["llrp:ROSpecID", "", "0"]]], 1, 0, "Delete All ROSpecs");


    my $ConfigKeepaliveTriggerType = "0";           # 0 - null, 1 - periodic
    my $ConfigPeriodicTriggerValue = 3000;          # only valid for periodic

    # C.f. Impinj Optimizing Tag Throughput Using ReaderMode
    #
    # Suggested order for testing: MaxMiller, DRM8, DRM4, Hybrid,
    #
    #my $ConfigC1G2RFControlModeIndex = "0";           # Max Throughput
    #my $ConfigC1G2RFControlModeIndex = "1";           # Hybrid
    #my $ConfigC1G2RFControlModeIndex = "2";           # Dense Reader M4
    #my $ConfigC1G2RFControlModeIndex = "3";           # Dense Reader M8
    #my $ConfigC1G2RFControlModeIndex = "4";           # Max Miller
    #my $ConfigC1G2RFControlModeIndex = "1000";        # Autoset Dense Reader
    my $ConfigC1G2RFControlModeIndex = "1001";        # Autoset Single Reader

    # Receive Sensitivity and Transmit Power
    # C.f. Impinj Setting Receive Sensitivity and Transmit Power on Revolution Reading using LLRP
    # C.f. Impinj If a Little Power is Good, Then More Must be Better, Right?
    #
    # Receiver Sensitivity - lower table numbers are more sensitive, (-80 dBM - ReceiverSensitivity)
    #   1 - ReceiverSensitivity 0, -80dBm
    #   2 - ReceiverSensitivity 10, -70 dBm
    #
    #   42 - ReceiverSensitivity 50, -30 dBm
    #
    # Transmit Power - Transmit Power divided by 100
    #   1 - TransmitPowerValue - 1000
    #   2 - TransmitPowerValue - 1025
    #   
    #   81 - TransmitPowerValue - 3000          # R420 default PoE
    #   91 - TransmitPowerValue - 3250          # R420 default 24VDC Power Supply
    # 
    #   61 - TransmitPowerValue - 3000          # R1000 
    #   71 - TransmitPowerValue - 3250          # R1000
    # 

    my $RFReceiverSensitivity = $ReceiverSensitivity;
    my $RFTransmitPower = $TransmitPowerMax;    # default 

    if ($readermode eq "Immediate") {
	$RFReceiverSensitivity = $ReceiverSensitivity;
        $RFTransmitPower = $TransmitPowerMax;
        $RFTransmitPower = 53; # no rental reads
        #$RFTransmitPower = 61;	# no reads
    }
    elsif ($readermode eq "One-Shot") {
        $RFReceiverSensitivity = "21";
        $RFTransmitPower = "11";
        #$RFReceiverSensitivity = "1";
        #$RFTransmitPower = $TransmitPowerMax;
    }

    syslog('warning', sprintf("Receive Sensitivity: %s dB (%s)\n", $ReceiveSensitivityTable[$RFReceiverSensitivity], $RFReceiverSensitivity));
    syslog('warning', sprintf("Transmit Power: %s dBm*100 (%s)\n", $TransmitPowerTable[$RFTransmitPower], $RFTransmitPower));

    # setup reader configuration
    #
    my $TagInventoryStateAware = "false";           # R420 does not support 
    dot($sock, 
        ["llrp:SET_READER_CONFIG", "MessageID=\"0\"", [
            ["llrp:ResetToFactoryDefault", "", "true"],
            ["llrp:ReaderEventNotificationSpec", "", [
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "Upon_Hopping_To_Next_Channel"],
                    ["llrp:NotificationState", "", "false"],
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "GPI_Event"],
                    ["llrp:NotificationState", "", "false"],
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "ROSpec_Event"],
                    ["llrp:NotificationState", "", "false"],     # generates start and finished events
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "Report_Buffer_Fill_Warning"],
                    ["llrp:NotificationState", "", "true"],
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "Reader_Exception_Event"],
                    ["llrp:NotificationState", "", "false"],
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "AISpec_Event"],
                    ["llrp:NotificationState", "", "true"],      # generates a single event
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "AISpec_Event"],
                    ["llrp:NotificationState", "", "false"],
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "AISpec_Event_With_Details"],
                    ["llrp:NotificationState", "", "true"],
                ]],
                ["llrp:EventNotificationState", "", [
                    ["llrp:EventType", "", "Antenna_Event"],
                    ["llrp:NotificationState", "", "true"],
                ]],
            ]],
            ["llrp:AntennaConfiguration", "", [
                ["llrp:AntennaID", "", "0"],
                # RFTransmitterSettings
                ["llrp:RFReceiver", "", [
                    ["llrp:ReceiverSensitivity", "", $RFReceiverSensitivity],
                    ]],
                ["llrp:RFTransmitter", "", [
                    ["llrp:HopTableID", "", "1"],
                    ["llrp:ChannelIndex", "", "0"],
                    ["llrp:TransmitPower", "", $RFTransmitPower],
                    ]],
                # AirProtocolInventorySettings
                ["llrp:C1G2InventoryCommand", "", [
                    #["C1G2Filter", "", [
                    #]],
                    ["llrp:TagInventoryStateAware", "", $TagInventoryStateAware],
                    ["llrp:C1G2RFControl", "", [
                        ["llrp:ModeIndex", "", $ConfigC1G2RFControlModeIndex ],
                        ["llrp:Tari", "", "0"],          # 
                    ]],
                    ["llrp:C1G2SingulationControl", "", [
                        ["llrp:Session", "", "1"],
                        ["llrp:TagPopulation", "", "100"],
                        ["llrp:TagTransitTime", "", "3000"],
                    ]],
                    # Single Target With Supression - maximum tag count
                    #["Impinj:ImpinjInventorySearchMode", "", [
                    #    ["Impinj:InventorySearchMode", "", "Single_Target_With_Suppression"],
                    #]],
                    # 
                    #["Impinj:ImpinjLowDutyCycle", "", [
                    #    ["Impinj:LowDutyCycleMode", "", "Enabled"],
                    #    ["Impinj:EmptyFieldTimeout", "", "1000"],
                    #    ["Impinj:FieldPingInterval", "", "4000"],
                    #]],
                ]],
            ]],
            ["llrp:KeepaliveSpec", "", [
                ["llrp:KeepaliveTriggerType", "", $ConfigKeepaliveTriggerType ],
                ["llrp:PeriodicTriggerValue", "", $ConfigPeriodicTriggerValue ],
            ]],
        ]], 0, 0, "Connfigure Reader Keepalive");

    dot($sock, 
        ["llrp:SET_READER_CONFIG", "MessageID=\"0\"", [
            ["llrp:ResetToFactoryDefault", "", "false"],
            ["llrp:AntennaConfiguration", "", [
                ["llrp:AntennaID", "", "1"],
                # RFTransmitterSettings
                ["llrp:RFReceiver", "", [
                    ["llrp:ReceiverSensitivity", "", $RFReceiverSensitivity],
                    ]],
                ["llrp:RFTransmitter", "", [
                    ["llrp:HopTableID", "", "1"],
                    ["llrp:ChannelIndex", "", "0"],
                    ["llrp:TransmitPower", "", $RFTransmitPower],
                    ]],
                # AirProtocolInventorySettings
                ["llrp:C1G2InventoryCommand", "", [
                    #["C1G2Filter", "", [
                    #]],
                    ["llrp:TagInventoryStateAware", "", $TagInventoryStateAware],
                    ["llrp:C1G2RFControl", "", [
                        ["llrp:ModeIndex", "", $ConfigC1G2RFControlModeIndex ],
                        ["llrp:Tari", "", "0"],          # 
                    ]],
                    ["llrp:C1G2SingulationControl", "", [
                        ["llrp:Session", "", "1"],
                        ["llrp:TagPopulation", "", "100"],
                        ["llrp:TagTransitTime", "", "3000"],
                    ]],
                    #["Impinj:ImpinjInventorySearchMode", "", [
                    #    ["Impinj:InventorySearchMode", "", "Single_Target_With_Suppression"],
                    #]],
                    # 
                    #["Impinj:ImpinjLowDutyCycle", "", [
                    #    ["Impinj:LowDutyCycleMode", "", "Enabled"],
                    #    ["Impinj:EmptyFieldTimeout", "", "1000"],
                    #    ["Impinj:FieldPingInterval", "", "4000"],
                    #]],
                ]],
            ]],
            ["llrp:AntennaConfiguration", "", [
                ["llrp:AntennaID", "", "2"],
                # RFTransmitterSettings
                ["llrp:RFReceiver", "", [
                    ["llrp:ReceiverSensitivity", "", $RFReceiverSensitivity],
                    ]],
                ["llrp:RFTransmitter", "", [
                    ["llrp:HopTableID", "", "1"],
                    ["llrp:ChannelIndex", "", "0"],
                    ["llrp:TransmitPower", "", $RFTransmitPower],
                    ]],
                # AirProtocolInventorySettings
                ["llrp:C1G2InventoryCommand", "", [
                    #["C1G2Filter", "", [
                    #]],
                    ["llrp:TagInventoryStateAware", "", $TagInventoryStateAware],
                    ["llrp:C1G2RFControl", "", [
                        ["llrp:ModeIndex", "", $ConfigC1G2RFControlModeIndex ],
                        ["llrp:Tari", "", "0"],          # 
                    ]],
                    ["llrp:C1G2SingulationControl", "", [
                        ["llrp:Session", "", "1"],
                        ["llrp:TagPopulation", "", "100"],
                        ["llrp:TagTransitTime", "", "3000"],
                    ]],
                    #["Impinj:ImpinjInventorySearchMode", "", [
                    #    ["Impinj:InventorySearchMode", "", "Single_Target_With_Suppression"],
                    #]],
                    # 
                    #["Impinj:ImpinjLowDutyCycle", "", [
                    #    ["Impinj:LowDutyCycleMode", "", "Enabled"],
                    #    ["Impinj:EmptyFieldTimeout", "", "1000"],
                    #    ["Impinj:FieldPingInterval", "", "4000"],
                    #]],
                ]],
            ]],
        ]], 0, 0, "Connfigure Reader Keepalive");


    return $sock;
}

# reader_shutdown
# 
sub reader::reader_shutdown {

    my ($sock) = @_;
    eval {
        dot($sock, ["llrp:DISABLE_ROSPEC", "MessageID=\"0\"", [["ROSpecID", "", "0"]]], 1, 0, "Disable All ROSpecs");
        dot($sock, ["llrp:DELETE_ROSPEC", "MessageID=\"0\"", [["ROSpecID", "", "0"]]], 1, 0, "Delete All ROSpecs");
    };
    eval {
        reader_disconnect ($sock);
    };
}
    
my $ROSpecID = "1";

# rospec_setup_immediate
# 
# Setup reader for periodic or one-shot operation
#
sub rospec_setup_immediate{

    my ($sock, $readermode) = @_;
    
    my $trace = 0;
    
    printf STDERR "%s ROSPEC Setup\n", $readermode;

    #my $AntennaIDs = "1 2 3 4";

    # Determine the RO Spec Boundary conditions, start and stop
    #
    my $ROSpecStartTrigger = "Null";       # requires a START_ROSPEC
    #my $ROSpecStartTrigger = "Immediate";   # will start immediately after ENABLE_ROSPEC
    #my $ROSpecStartTrigger = "Periodic";   # will run periodically after wait period
    #my $ROSpecStartTrigger = "GPI";        # will start when GPI signal received

    #my $ROSpecStopTrigger = "Null";         # don't stop
    #my $ROSpecStopTrigger = "Immediate";
    #my $ROSpecStopTrigger = "Periodic";
    #my $ROSpecStopTrigger = "GPI";

    # Define the Stop, terminating boundary of an antenna inventory operation
    #
    my $AISpecStopTriggerType = "Null";
    #my $AISpecStopTriggerType = "Duration";
    #my $AISpecStopTriggerType = "GPI_With_Timeout";
    #my $AISpecStopTriggerType = "Tag_Observation";          # requires TagObservationTrigger

    # TagObservation Trigger
    #
    my $AISpecTriggerType = "Upon_Seeing_N_Tags_Or_Timeout";
    #my $AISpecTriggerType = "Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout";
    #my $AISpecTriggerType = "N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout";

    # Report
    #
    my $ROReportTrigger = "None";
    #my $ROReportTrigger = "Upon_N_Tags_Or_End_Of_AISpec";
    #my $ROReportTrigger = "Upon_N_Tags_Or_End_Of_ROSpec";
    #my $ROReportN = 0;

    my $ROSpecStopTrigger = "Null";         # don't stop
    my $ROSpecStopDuration = "0";

    # Define the Stop, terminating boundary of an antenna inventory operation
    #
    my $AISpecDurationTrigger = "0";                        # valid only for Duration

    # TagObservation Trigger
    #

    my $AISpecStopNumberOfTags = "50";      # only valid for Upon_Seeing_N_Tags_Or_Timeout
    my $AISpecStopNumberOfAttempts = "1";   # only valid for N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout
    my $AISpecStopT = "0";                  # only valid for Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout
    my $AISpecStopTimeout = "2000";         # timeout in mS or zero for no timeout

    # Report
    #
    my $ROReportN = 0;



    # RFTransmitter / TranmitterPwer - default 81 - TransmitPowerValue - 3000
    if ($readermode eq "Immediate") {

        # Determine the RO Spec Boundary conditions, start and stop
        #
        $ROSpecStartTrigger = "Immediate";   # will start immediately after ENABLE_ROSPEC
        $ROSpecStopTrigger = "Null";         # don't stop
        $ROSpecStopDuration = "0";

        # Define the Stop, terminating boundary of an antenna inventory operation
        #
        $AISpecStopTriggerType = "Tag_Observation";          # requires TagObservationTrigger
        $AISpecDurationTrigger = "0";                        # valid only for Duration

        # TagObservation Trigger
        #
        #$AISpecTriggerType = "Upon_Seeing_N_Tags_Or_Timeout";
        #$AISpecTriggerType = "N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout";
        $AISpecTriggerType = "Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout";

        $AISpecStopNumberOfTags = "50";      # only valid for Upon_Seeing_N_Tags_Or_Timeout
        $AISpecStopNumberOfAttempts = "1";   # only valid for N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout
        $AISpecStopT = "1000";                  # only valid for Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout
        $AISpecStopTimeout = "1000";         # timeout in mS or zero for no timeout

        # Report
        #
        $ROReportTrigger = "Upon_N_Tags_Or_End_Of_ROSpec";
        $ROReportN = 0;
    }

    if ($readermode eq "One-Shot") {

        # Determine the RO Spec Boundary conditions, start and stop
        #
        $ROSpecStartTrigger = "Null";        # requires a START_ROSPEC
        $ROSpecStopTrigger = "Null";         # don't stop
        $ROSpecStopDuration = "0";

        # Define the Stop, terminating boundary of an antenna inventory operation
        #
        $AISpecStopTriggerType = "Tag_Observation";          # requires TagObservationTrigger
        $AISpecDurationTrigger = "0";                        # valid only for Duration

        # TagObservation Trigger
        #
        $AISpecTriggerType = "Upon_Seeing_N_Tags_Or_Timeout";

        $AISpecStopNumberOfTags = "50";      # only valid for Upon_Seeing_N_Tags_Or_Timeout
        $AISpecStopNumberOfAttempts = "1";   # only valid for N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout
        $AISpecStopT = "0";                  # only valid for Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout
        $AISpecStopTimeout = "200";         # timeout in mS or zero for no timeout

        # Report
        #
        $ROReportTrigger = "Upon_N_Tags_Or_End_Of_ROSpec";
        $ROReportN = 0;

    }


    if ($ModelName eq "Speedway R420") {
        dot($sock, 
            ["llrp:ADD_ROSPEC","MessageID=\"0\"", [
                ["llrp:ROSpec", "", [
                    ["llrp:ROSpecID", "", $ROSpecID],
                    ["llrp:Priority", "", "0"],
                    ["llrp:CurrentState", "", "Disabled"],
                    ["llrp:ROBoundarySpec", "", [
                        ["llrp:ROSpecStartTrigger", "", [
                            ["llrp:ROSpecStartTriggerType", "", $ROSpecStartTrigger],
                            #["PeriodicTriggerValue", "", [
                            #    ["Offset", "", "0"],
                            #    ["Period", "", "0"],
                            #]]
                        ]],
                        ["llrp:ROSpecStopTrigger", "", [
                            ["llrp:ROSpecStopTriggerType", "", $ROSpecStopTrigger],
                            ["llrp:DurationTriggerValue", "", $ROSpecStopDuration],
                        ]],
                    ]],
                    ["llrp:AISpec", "", [
                        ["llrp:AntennaIDs", "", $AntennaIDs],
                        ["llrp:AISpecStopTrigger", "", [
                            ["llrp:AISpecStopTriggerType", "", $AISpecStopTriggerType],
                            ["llrp:DurationTrigger", "", $AISpecDurationTrigger],
                            ["llrp:TagObservationTrigger", "", [
                                ["llrp:TriggerType", "", $AISpecTriggerType],
                                ["llrp:NumberOfTags", "", $AISpecStopNumberOfTags],
                                ["llrp:NumberOfAttempts", "", $AISpecStopNumberOfAttempts],
                                ["llrp:T", "", $AISpecStopT],
                                ["llrp:Timeout", "", $AISpecStopTimeout],
                            ]],
                        ]],
                        ["llrp:InventoryParameterSpec", "", [
                            ["llrp:InventoryParameterSpecID", "", "1234"],
                            ["llrp:ProtocolID", "", "EPCGlobalClass1Gen2"],
                        ]],
                    ]],
                    ["llrp:ROReportSpec", "", [
                        ["llrp:ROReportTrigger", "", $ROReportTrigger],
                        ["llrp:N", "", $ROReportN],
                        ["llrp:TagReportContentSelector", "", [
                            ["llrp:EnableROSpecID", "", "true"],
                            ["llrp:EnableSpecIndex", "", "true"],
                            ["llrp:EnableInventoryParameterSpecID", "", "true"],
                            ["llrp:EnableAntennaID", "", "true"],
                            ["llrp:EnableChannelIndex", "", "true"],
                            ["llrp:EnablePeakRSSI", "", "true"],
                            ["llrp:EnableFirstSeenTimestamp", "", "true"],
                            ["llrp:EnableLastSeenTimestamp", "", "true"],
                            ["llrp:EnableTagSeenCount", "", "true"],
                            ["llrp:EnableAccessSpecID", "", "true"],
                            #["llrp:C1G2EPCMemorySelector", "", [
                            #        ["llrp:EnableCRC", "", "1"],
                            #        ["llrp:EnablePCBits", "", "1"],
                            #]],
                        ]],
                        #["Impinj:ImpinjTagDirectionReporting", "", [
                        #    ["Impinj:EnableTagDirection","","True"],
                        #    ["Impinj:AntennaConfiguration","","Dual_Antenna"],
                        #]],
                        ["Impinj:ImpinjTagReportContentSelector", "", [
                            ["Impinj:ImpinjEnableRFPhaseAngle", "", [
                                ["Impinj:RFPhaseAngleMode", "", "Enabled"],
                            ]],
                            ["Impinj:ImpinjEnablePeakRSSI", "", [
                                ["Impinj:PeakRSSIMode", "", "Enabled"],
                            ]],
                        ]],
                    ]],
                ]],
            ]], 1, 0, "Add ROSpec");
        }
    else {
        dot($sock, 
            ["llrp:ADD_ROSPEC","MessageID=\"0\"", [
                ["llrp:ROSpec", "", [
                    ["llrp:ROSpecID", "", $ROSpecID],
                    ["llrp:Priority", "", "0"],
                    ["llrp:CurrentState", "", "Disabled"],
                    ["llrp:ROBoundarySpec", "", [
                        ["llrp:ROSpecStartTrigger", "", [
                            ["llrp:ROSpecStartTriggerType", "", $ROSpecStartTrigger],
                            #["PeriodicTriggerValue", "", [
                            #    ["Offset", "", "0"],
                            #    ["Period", "", "0"],
                            #]]
                        ]],
                        ["llrp:ROSpecStopTrigger", "", [
                            ["llrp:ROSpecStopTriggerType", "", $ROSpecStopTrigger],
                            ["llrp:DurationTriggerValue", "", $ROSpecStopDuration],
                        ]],
                    ]],
                    ["llrp:AISpec", "", [
                        ["llrp:AntennaIDs", "", $AntennaIDs],
                        ["llrp:AISpecStopTrigger", "", [
                            ["llrp:AISpecStopTriggerType", "", $AISpecStopTriggerType],
                            ["llrp:DurationTrigger", "", $AISpecDurationTrigger],
                            ["llrp:TagObservationTrigger", "", [
                                ["llrp:TriggerType", "", $AISpecTriggerType],
                                ["llrp:NumberOfTags", "", $AISpecStopNumberOfTags],
                                ["llrp:NumberOfAttempts", "", $AISpecStopNumberOfAttempts],
                                ["llrp:T", "", $AISpecStopT],
                                ["llrp:Timeout", "", $AISpecStopTimeout],
                            ]],
                        ]],
                        ["llrp:InventoryParameterSpec", "", [
                            ["llrp:InventoryParameterSpecID", "", "1234"],
                            ["llrp:ProtocolID", "", "EPCGlobalClass1Gen2"],
                        ]],
                    ]],
                    ["llrp:ROReportSpec", "", [
                        ["llrp:ROReportTrigger", "", $ROReportTrigger],
                        ["llrp:N", "", $ROReportN],
                        ["llrp:TagReportContentSelector", "", [
                            ["llrp:EnableROSpecID", "", "true"],
                            ["llrp:EnableSpecIndex", "", "true"],
                            ["llrp:EnableInventoryParameterSpecID", "", "true"],
                            ["llrp:EnableAntennaID", "", "true"],
                            ["llrp:EnableChannelIndex", "", "true"],
                            ["llrp:EnablePeakRSSI", "", "true"],
                            ["llrp:EnableFirstSeenTimestamp", "", "true"],
                            ["llrp:EnableLastSeenTimestamp", "", "true"],
                            ["llrp:EnableTagSeenCount", "", "true"],
                            ["llrp:EnableAccessSpecID", "", "true"],
                            #["llrp:C1G2EPCMemorySelector", "", [
                            #        ["llrp:EnableCRC", "", "1"],
                            #        ["llrp:EnablePCBits", "", "1"],
                            #]],
                        ]],
                    ]],
                ]],
            ]], 1, 0, "Add ROSpec");
        }
}

# rospec_setup_periodic
# 
# Setup reader for periodic or one-shot operation
#
sub rospec_setup_periodic{

    my ($sock, $readermode) = @_;
    
    my $trace = 0;
    
    printf STDERR "%s ROSPEC Setup - Low Duty\n", $readermode;

    #my $AntennaIDs = "1 2 3 4";

    # Determine the RO Spec Boundary conditions, start and stop
    #
    my $ROSpecStartTrigger = "Null";       # requires a START_ROSPEC
    #my $ROSpecStartTrigger = "Immediate";   # will start immediately after ENABLE_ROSPEC
    #my $ROSpecStartTrigger = "Periodic";   # will run periodically after wait period
    #my $ROSpecStartTrigger = "GPI";        # will start when GPI signal received

    #my $ROSpecStopTrigger = "Null";         # don't stop
    #my $ROSpecStopTrigger = "Immediate";
    #my $ROSpecStopTrigger = "Periodic";
    #my $ROSpecStopTrigger = "GPI";

    # Define the Stop, terminating boundary of an antenna inventory operation
    #
    my $AISpecStopTriggerType = "Null";
    #my $AISpecStopTriggerType = "Duration";
    #my $AISpecStopTriggerType = "GPI_With_Timeout";
    #my $AISpecStopTriggerType = "Tag_Observation";          # requires TagObservationTrigger

    # TagObservation Trigger
    #
    my $AISpecTriggerType = "Upon_Seeing_N_Tags_Or_Timeout";
    #my $AISpecTriggerType = "Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout";
    #my $AISpecTriggerType = "N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout";

    # Report
    #
    my $ROReportTrigger = "None";
    #my $ROReportTrigger = "Upon_N_Tags_Or_End_Of_AISpec";
    #my $ROReportTrigger = "Upon_N_Tags_Or_End_Of_ROSpec";
    #my $ROReportN = 0;

    my $ROSpecStopTrigger = "Null";         # don't stop
    my $ROSpecStopDuration = "0";

    # Define the Stop, terminating boundary of an antenna inventory operation
    #
    my $AISpecDurationTrigger = "0";                        # valid only for Duration

    # TagObservation Trigger
    #

    my $AISpecStopNumberOfTags = "50";      # only valid for Upon_Seeing_N_Tags_Or_Timeout
    my $AISpecStopNumberOfAttempts = "1";   # only valid for N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout
    my $AISpecStopT = "0";                  # only valid for Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout
    my $AISpecStopTimeout = "2000";         # timeout in mS or zero for no timeout

    # Report
    #
    my $ROReportN = 0;

    # RFTransmitter / TranmitterPwer - default 81 - TransmitPowerValue - 3000
    if ($readermode eq "Immediate") {

        # Determine the RO Spec Boundary conditions, start and stop
        #
        $ROSpecStartTrigger = "Periodic";    # will start periodically after ENABLE_ROSPEC
        $ROSpecStopTrigger = "Null";         # don't stop
        $ROSpecStopDuration = "0";

        # Define the Stop, terminating boundary of an antenna inventory operation
        #
        $AISpecStopTriggerType = "Tag_Observation";          # requires TagObservationTrigger
        $AISpecDurationTrigger = "0";                        # valid only for Duration

        # TagObservation Trigger
        #
        $AISpecTriggerType = "Upon_Seeing_N_Tags_Or_Timeout";

        $AISpecStopNumberOfTags = "50";      # only valid for Upon_Seeing_N_Tags_Or_Timeout
        $AISpecStopNumberOfAttempts = "1";   # only valid for N_Attempts_To_See_All_Tags_In_FOV_Or_Timeout
        $AISpecStopT = "0";                  # only valid for Upon_Seeing_No_More_New_Tags_For_Tms_Or_Timeout
        $AISpecStopTimeout = "2000";         # timeout in mS or zero for no timeout

        # Report
        #
        $ROReportTrigger = "Upon_N_Tags_Or_End_Of_ROSpec";
        $ROReportN = 0;
    }

    dot($sock, 
        ["llrp:ADD_ROSPEC","MessageID=\"0\"", [
            ["llrp:ROSpec", "", [
                ["llrp:ROSpecID", "", $ROSpecID],
                ["llrp:Priority", "", "0"],
                ["llrp:CurrentState", "", "Disabled"],
                ["llrp:ROBoundarySpec", "", [
                    ["llrp:ROSpecStartTrigger", "", [
                        ["llrp:ROSpecStartTriggerType", "", "Periodic"],
                        ["llrp:PeriodicTriggerValue", "", [
                            ["llrp:Offset", "", "0"],
                            ["llrp:Period", "", "2000"],
                        ]]
                    ]],
                    ["llrp:ROSpecStopTrigger", "", [
                        ["llrp:ROSpecStopTriggerType", "", "Duration"],
                        ["llrp:DurationTriggerValue", "", 100],
                    ]],
                ]],
                ["llrp:AISpec", "", [
                    ["llrp:AntennaIDs", "", $AntennaIDs],
                    ["llrp:AISpecStopTrigger", "", [
                        ["llrp:AISpecStopTriggerType", "", "Null"],
                        ["llrp:DurationTrigger", "", 50],
                    ]],
                    ["llrp:InventoryParameterSpec", "", [
                        ["llrp:InventoryParameterSpecID", "", "1234"],
                        ["llrp:ProtocolID", "", "EPCGlobalClass1Gen2"],
                    ]],
                ]],
                ["llrp:ROReportSpec", "", [
                    ["llrp:ROReportTrigger", "", $ROReportTrigger],
                    ["llrp:N", "", $ROReportN],
                    ["llrp:TagReportContentSelector", "", [
                        ["llrp:EnableROSpecID", "", "true"],
                        ["llrp:EnableSpecIndex", "", "true"],
                        ["llrp:EnableInventoryParameterSpecID", "", "true"],
                        ["llrp:EnableAntennaID", "", "true"],
                        ["llrp:EnableChannelIndex", "", "true"],
                        ["llrp:EnablePeakRSSI", "", "true"],
                        ["llrp:EnableFirstSeenTimestamp", "", "true"],
                        ["llrp:EnableLastSeenTimestamp", "", "true"],
                        ["llrp:EnableTagSeenCount", "", "true"],
                        ["llrp:EnableAccessSpecID", "", "true"],
                    ]],
                ]],
            ]],
        ]], 1, 0, "Add ROSpec");
}

sub reader::rospec_setup{

    my ($sock, $dbsql, $readername, $readermode, $lowdutyflag, $servermode) = @_;
    
    printf STDERR "%s ROSPEC Setup: lowduty: %d \n", $readermode, $lowdutyflag;

    eval {
        dot($sock, ["llrp:DISABLE_ROSPEC", "MessageID=\"0\"", [["llrp:ROSpecID", "", "0"]]], 1, 0, "Disable All ROSpecs");
        dot($sock, ["llrp:DELETE_ROSPEC", "MessageID=\"0\"", [["llrp:ROSpecID", "", "0"]]], 1, 0, "Delete All ROSpecs");
    };

    if ($readermode eq "One-Shot") {
        rospec_setup_immediate($sock, $readermode);
        syslog('warning', sprintf("One-Shot ROSPEC %s", $readername)) if ($servermode);
    }
    elsif ($readermode eq "Immediate" && $lowdutyflag) {

        # generate a lap header line anytime we switch to low duty mode, this sends a signal
        # to lapimport that it should flush all workouts / groups / races because nobody
        # is currently using the track.
        #
        tags::lapdheader();

        # setup low-duty rospec, this poll's the antennas about once per second instead of 
        # running them continously.
        #
        rospec_setup_periodic($sock, $readermode);
        syslog('warning', sprintf("Low-Duty ROSPEC %s", $readername)) if ($servermode);
    }
    elsif ($readermode eq "Immediate" && !$lowdutyflag) {

        # setup full-duty mode, this runs the readers continuously for all connected antennas
        # with results collected and forwarded back once per second
        #
        rospec_setup_immediate($sock, $readermode); 
        syslog('warning', sprintf("Full-Duty ROSPEC %s", $readername)) if ($servermode);
    }
    dot($sock, ["llrp:ENABLE_ROSPEC", "MessageID=\"0\"",[ ["ROSpecID", "", $ROSpecID] ]], 1, 0, "Enable ROSpec");

}


my $rospecstarted = 0;
sub reader::rospec_started {
    $rospecstarted = 0;
}
sub reader::rospec_start {
    my ($sock, $msg) = @_;
    return if ($rospecstarted);
    #printf STDERR "\nStart ROSPEC %s\n", $msg;
    $rospecstarted = 1;
    dot($sock, ["llrp:START_ROSPEC", "MessageID=\"0\"",[ ["ROSpecID", "", $ROSpecID] ]], 1, 0, "Enable ROSpec");
}

###################################################################################################################### #


my $LastTagRead = Time::HiRes::gettimeofday();
sub reader::getlasttagread {
    return $LastTagRead;
}


sub reader::process_reader_responses {

    my ($sock, $dbsql, $readermode, $trackmode, $servermode) = @_;


    # Safely read a message and decode it
    #
    my $ntf;
    eval {
        my $buf = read_message($sock, 20);
        my %options;
        my $tree;
        my @decode_opt = ('HashParent' => $tree = {});
        $ntf = decode_message($buf, %options, @decode_opt, QualifyCore => 0);
    };

    unless (defined($ntf)) {
        printf STDERR "Timed out\n";
        #$InterruptedFlag = 0;
        return;
    }

    my (@timestamps);

    #printf STDERR "PROCESS: %s\n", $ntf->toString();
    if ($ntf->exists("//READER_EVENT_NOTIFICATION/node()")) {
        printf STDERR "READER_EVENT_NOTIFICATION FOUND\n";

        if ($ntf->exists("//AISpecEvent/node()")) {
            printf STDERR "AISpecEvent FOUND\n";

            my @timestamp_nodes = $ntf->findnodes("//UTCTimestamp/Microseconds/node()");
            for (my $i = 0; $i <= $#timestamp_nodes; $i++) {

                $timestamps[$i] = $timestamp_nodes[$i]->textContent; 

                my $timestamp = $timestamps[$i];

                #printf STDERR "[%3d] %s\n", $i, $timestamp;

                my $epoch = gmtoffset::getepoch($timestamp, 0);
                my $endtime = Time::HiRes::gettimeofday();

                my $dt = DateTime->from_epoch(epoch =>$epoch);
                #printf STDERR "Reader UTC: %s\n",  $dt->strftime("%Y-%m-%d %H:%M:%S", tz => "PST8PDT");

                $dt = DateTime->from_epoch(epoch => ($epoch + gmoffset::getgmtoffset()));
                #printf STDERR "Reader PST: %s %s\n",  $dt->strftime("%Y-%m-%d %H:%M:%S", tz => "PST8PDT"), $epoch + gmoffset::getgmtoffset();

                $dt = DateTime->from_epoch(epoch => ($endtime + gmoffset::getgmtoffset()));
                #printf STDERR "Local  PST: %s %s\n",  $dt->strftime("%Y-%m-%d %H:%M:%S", tz => "PST8PDT"), $endtime + gmoffset::getgmtoffset();

                push(@Reader_Offsets, $epoch - $endtime);

                if ($#Reader_Offsets > 10) {
                    shift(@Reader_Offsets);
                }

                $Reader_Offset = mean(@Reader_Offsets);
                #printf STDERR "Offset: %s [%d]\n", $Reader_Offset, $#Reader_Offsets;
            }
        }
        elsif ($ntf->exists("//Report_Buffer_Fill_Warning/node()")) {
            printf STDERR "Report_Buffer_Fill_Warning FOUND\n";
        }
        elsif ($ntf->exists("//Antenna_Disconnected/node()")) {
            printf STDERR "Antenna_Disconnected FOUND\n";
        }
        else {
            printf STDERR "Unknown event: %s\n", $ntf->toString();
        }
        return;
    }
    if ($ntf->exists("//RO_ACCESS_REPORT/node()")){
        #printf STDERR "RO_ACCESS_REPORT FOUND\n";
        #printf STDERR "%s\n", $ntf->toString();
        my (@ecps, @antennas, @peakrssis, @firstseens, @lasttseens, @tagseens);
        my (@epochs, @epoch_times);

        $LastTagRead = Time::HiRes::gettimeofday();

        my @ecps_nodes = $ntf->findnodes("//EPC/node()");
        my @antenna_nodes = $ntf->findnodes("//AntennaID/AntennaID/node()");
        my @peakrssi_nodes = $ntf->findnodes("//PeakRSSI/PeakRSSI/node()");
        my @firstseen_nodes = $ntf->findnodes("//FirstSeenTimestampUTC/Microseconds/node()");
        my @lastseen_nodes = $ntf->findnodes("//LastSeenTimestampUTC/Microseconds/node()");
        my @tagseen_nodes = $ntf->findnodes("//TagSeenCount/TagCount/node()");

        my %tagtimes;
        my %tagepochs;
        my %tagcounts;
        my %tagrssis;
        my %tagantennas;

        for (my $i = 0; $i <= $#ecps_nodes; $i++) {
            my $tag = $ecps_nodes[$i]->textContent; 
            #$epcs[$i] = $tag;

            $antennas[$i] = $antenna_nodes[$i]->textContent; 
            $peakrssis[$i] = $peakrssi_nodes[$i]->textContent; 
            $firstseens[$i] = $firstseen_nodes[$i]->textContent; 
            #$lastseens[$i] = $lastseen_nodes[$i]->textContent; 
            $tagseens[$i] = $tagseen_nodes[$i]->textContent; 

            $epochs[$i] = gmtoffset::getepoch($firstseens[$i], (gmoffset::getgmtoffset() - $Reader_Offset));

            $tagepochs{$tag} = $epochs[$i];
            $tagcounts{$tag}++;
            if (defined($tagrssis{$tag})) {
                printf STDERR "DOUBLE previous: %d now: %d\n", $tagantennas{$tag}, $antennas[$i];
                if ($tagrssis{$tag} < $peakrssis[$i]) {
                    $tagrssis{$tag} = $peakrssis[$i];
                    $tagantennas{$tag} = $antennas[$i];
                }
            }
            else {
                $tagrssis{$tag} = $peakrssis[$i];
                $tagantennas{$tag} = $antennas[$i];
            }

            my $dt = DateTime->from_epoch(epoch =>$epochs[$i]);
            $tagtimes{$tag} = $dt->strftime("%Y-%m-%d %H:%M:%S.%3N");

            #$epochs_time[$i] = $dt->strftime("%Y-%m-%d %H:%M:%S.%3N");

            #printf STDERR "[%3d] %8s %2s %5s %5d %s\n", $i, $epcs[$i], $antennas[$i], $peakrssis[$i], $tagseens[$i], $epochs_time[$i];
        }

        foreach my $key (sort {$tagtimes{$a} cmp $tagtimes{$b}} keys %tagtimes) {
            printf STDERR "%8s %5s %d %d %s\n", $key, $tagrssis{$key}, $tagcounts{$key}, $tagantennas{$key}, $tagtimes{$key};
        }
        my @csvlines;
        my %tags;
        foreach my $key (sort {$tagtimes{$a} cmp $tagtimes{$b}} keys %tagtimes) {
            push(@csvlines, service::do_process ($dbsql, $RxID++, $key, $tagtimes{$key}, $tagepochs{$key}, $tagantennas{$key}, $tagrssis{$key}, $readermode, $trackmode, $servermode));
            my $antenna = $tagantennas{$key};
            my $times = $tagtimes{$key};

            #$tags{}
        }
        #$RxID++;
        return;
    }
    if ($ntf->exists("//RO_ACCESS_REPORT")){
        #printf STDERR "EMPTY RO_ACCESS_REPORT FOUND\n";
        return;
    }
    printf STDERR "NOTHING FOUND\n";
    printf STDERR "%s\n", $ntf->toString();

}


1;

