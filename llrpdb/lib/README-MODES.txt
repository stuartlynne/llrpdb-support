Impinj Inventory Search Modes                                   Stuart Lynne
Wimsey                                          Sun Sep 27 12:24:59 PDT 2015 

From: http://www.brettgreen.com/llrpsource/Documents/InventorySearchMode.pdf

Inventory Search Mode       Session         Comments
    Dual Target             Session 2       Session persistence does not impact reading as the
                                            reader will automatically move tags between A and B to
                                            optimized reading tags repeatedly.  This configuration
                                            is useful in static configurations when you want to know
                                            a tag is no longer being read (e.g.  a shelf
                                            application)

    Single Target           Session 1       The reader will inventory for only A tags and then
                                            move tags that it has read from A to B.  Because we are
                                            using Session 1 the tags will fall back to A from B
                                            between 500 ms and 5 seconds when the tags are energized
                                            or not energized.  This configuration is good in dynamic
                                            situations where you want to reduce the number of times
                                            you re‐read tags because you want to focus on tags that
                                            you haven’t read yet.  It also allows you to re‐read
                                            tags at another read point or during another inventory
                                            run soon after because the tags will fall back to A from
                                            B within 5 seconds.  

    Single Target          Session 2        This configuration is the same as
                                            “Single Target” with “Session 1” with the exception that
                                            because you are in Session 2 the tags will have a larger
                                            and variable amount of time (10 seconds to minutes) for
                                            which they will return to A from B.  This is bad if you
                                            want to re‐read the tags again as part of another
                                            inventory run or you want to read them at another read
                                            point soon after they have been read.  It is good if you
                                            only need to read them once within a number of
                                            minutes.  Example:  Read once at dock door and put away.

    Single Target w/Suppression Session 1   Single Target w/ Suppression is the best of both
                                            worlds.  Greatly reducing the number of times you
                                            re‐read tags like when you use Single Target with
                                            Session 2 but without having your operation be
                                            negatively impacted by the persistence time of Session 2.


By default the reader is in “Reader Selected” mode for selecting the Inventory Search Mode being
used.  The reader bases the Inventory Search Mode it will use on the Session that is being
used.  You can manually set the Inventory Search Mode by using the Impinj LLRP vendor extension
parameter “ImpinjInventorySearchMode” to SET_READER_CONFIG or as part of individual AISpecs within a
ROSpec.  You can also set a static Inventory Search Mode that will persist across reboots through
the web page on the reader (Configuration ‐> RFID ‐> LLRP ‐> Default Inventory Search Mode).

Reader Selected Defaults per Session
Session     Inventory Search Mode
Session 1   Single Target
Session 2   Dual Target
Session 3   Dual Target


