#!/usr/bin/perl
# vim60:fdm=marker:
#
# LLRPDB - Implement a service to read tags from an LLRP RFID Reader
#
# This script is designed to support using an LLRP RFID Reader for doing Bike Races.
#
# Operational modes
# *****************
#
# Registration
#       Operate in one-shot mode to interrogate the antennas and report tags to
#       interested parties via a TCP connection. Ports 5551-5554 are used, one
#       for each antenna. The Transmit power and Receive sensitivity are lowered
#       to keep the hand held antennas read zone small.
#
# Race
#       Operate in continuous mode to interrogate the antennas and report the 
#       tags interested parties via a TCP connection. Port 5550 is used.
#
# Track
#       The same as race except that if no tags are seen for more than 15 minutes
#       the reader is configured to a low-duty cycle. Typically this means that it
#       will operate for about 50 mS once every 1000 mS until a tag is read. Then
#       it will revert to 100% continuous operation.
#
# CrossMgr
#       The same as race, but additionally will attempt to connect to CrossMgr to
#       report tag data.
#
#
# AutoConfiguration
# *****************
#
# Reader Offset
#       The service monitors the time offset from the server. This is used to adjust
#       the observed time to the local time.
#
# Antenna Check
#       The service will perform an antenna check every two seconds. If the antenna
#       configuration changes it will disconnect then do a new connection and configuration
#       sequence.
#
# Disconnect Check
#       The service will timeout after two seconds of network loss. It will then attempt
#       to reconnect every five seconds until connected.
#
# Impinj Extensions
# *****************
#
# The xmlns definitions are inserted in the top level of all transactions to allow for
# proper encoding of llrp vs Impinj messages and options:
#       xmlns:llrp=\"http://www.llrp.org/ltk/schema/core/encoding/xml/1.0\" 
#       xmlns:Impinj=\"http://developer.impinj.com/ltk/schema/encoding/xml/1.8\" ";
#
# This allows for definitions such as:
#
#       <Impinj:IMPINJ_ENABLE_EXTENSIONS>
#


use strict;
use warnings;

use lib "./bin/";

use Getopt::Long;
use Pod::Usage qw(pod2usage);

use DBI;

use Getopt::Std;
use IO::Select;
use IO::Socket;
use IO::Socket::INET;
use Socket qw(IPPROTO_TCP SOL_SOCKET SO_LINGER SO_KEEPALIVE inet_aton sockaddr_in TCP_NODELAY);
use Socket::Linux qw(TCP_NODELAY TCP_KEEPIDLE TCP_KEEPINTVL TCP_KEEPCNT);

use Time::CTime;
use Sys::Syslog qw (:standard :macros);

use DateTime;
use DateTime::TimeZone;
use DateTime::Locale;
use DateTime::Format::Strptime;
use Data::Dumper;

use Date::Parse;
use Data::HexDump;
use List::Util qw(sum);

use Time::Local;
use Time::HiRes qw(time gettimeofday);

#use XML::LibXML;

use RFID::LLRP::Builder qw(encode_message decode_message);
use RFID::LLRP::Link qw(
        reader_connect
        reader_disconnect
        read_message
        ),
    transact => {QualifyCore => 0},
    monitor => {QualifyCore => 0};

use RFID::LLRP::Helper qw(delete_rospecs are_identical delete_access_specs
        factory_default cleanup_all compare_subtrees);

#use lib::gmoffset qw(getlocaloffset getgmtoffset getepoch);

use lib::service qw(do_service);
use lib::SimpleCSV qw(init parse);
use lib::tags qw(record_tag);

use diagnostics;

#my $GMTOffset = 0;

# ################################################################################################################### #


# open log
openlog($0, "ndelay,pid", "daemon");



sub HELP_MESSAGE {
    printf STDERR "Usage: llrpdb [venue] | [ipaddr port [hostname]]\n";
    printf STDERR "\tvenue - read WT Ative RACE CSV file on STDIN, output processed data on STDOUT\n";
    printf STDERR "\tipaddr port - connect to ipaddr:port, save in /var/racereplay/laps.*csv\n";
    printf STDERR "\tipaddr port hostname - connect to ipaddr:port, forward to hostname\n";
    exit;
}
sub VERSION_MESSAGE {
    printf STDERR "version 1.0\n";
}


###################################################################################################################### #


my ($help, $man, $debug, $dump, $testcsv);
my ($RaceDBPath, $LapdPath, $ReaderName);

$testcsv = 0;

GetOptions(
        'help|?' => \$help,
        'man' => \$man,
        'debug|d' => \$debug,
        'testcsv|t' => \$testcsv,
        'dump+' => \$dump,

        'lapdpath=s' => \$LapdPath,
        'racedbpath=s' => \$RaceDBPath,
        'readername=s' => \$ReaderName,

) || pod2usage(2);;

pod2usage(1) if $help;


#pod2usage(-exitval => 0, -verbose => 2) if $man;
#pod2usage(-verbose => 2) if $man;
#pod2usage(-verbose => 2);



pod2usage(1) unless(defined($LapdPath) && defined($RaceDBPath));
printf STDERR "LapdPath: %s\n", $LapdPath;
printf STDERR "RaceDBPath: %s\n", $RaceDBPath;
printf STDERR "testcsv: %d\n", $testcsv;
printf STDERR "ReaderName: %s\n", $ReaderName if (defined $ReaderName);

pod2usage(1) unless(defined($ReaderName) || $testcsv);


###################################################################################################################### #

sub testcsv {

    my ($dbsql) = @_;

    printf STDERR "testcsv\n";


    my $rcsv = SimpleCSV::init('test');

    # iterate across all files specified as arguements
    #
    my $firstflag = 1;
    my $count = 0;
    while (<ARGV>) {
       my $row;

        printf STDERR $_;
        printf STDERR "\n\n[%d]-----------------------------------------\n%s", $count++, $_;

        # initialize prior to each file
        #
        if ($firstflag) {
            $firstflag = 0;
            $row = SimpleCSV::parse($rcsv, $_, 1);
            next;
        }

        # process current line
        #
        $row = SimpleCSV::parse($rcsv, $_, 0);



        unless (defined($row) && $row != 0) {
            next;
        }


        my $rxid = $row->{'rx'};
        my $tagid = $row->{'tagid'}; 
        my $timestamp = $row->{'datestamp'};
        my $epoch = $row->{'finishms'} / 1000;
        my $antenna = $row->{'antenna'};
        my $rssi = $row->{'rssi'};
        my $readermode = 0;
        my $trackmode = 1;
        my $servermode = 0;

        my $csvline = tags::record_tag(
                
                $dbsql, 
                $rxid++, 
                $tagid, 
                $timestamp, 
                $epoch, 
                $antenna, 
                $rssi, 

                $readermode, 
                $trackmode, 
                $servermode
                );


        #$printf STDERR Dumper($row); 
 
    }

}


###################################################################################################################### #

chdir ($LapdPath) || die "Cannot chdir to $LapdPath";

my $DBSql = DBI->connect("dbi:SQLite:dbname=$RaceDBPath","","",{'PrintError' => 0});
die "Cannot open database: " . $RaceDBPath . "\n" unless ($DBSql);


if ($testcsv) {
    testcsv($DBSql);
    exit(0);
}

my $ReaderMode = "Immediate";;
#my $ReaderMode = "One-Shot";;

printf STDERR "\$0: %s\n", $0;

if ($0 =~ /race$/) {
    $ReaderMode = "Immediate";
    printf STDERR "Immediate mode\n";
}
elsif ($0 =~ /track$/) {
    $ReaderMode = "Low-Duty";
    printf STDERR "Low-Duty mode\n";
}
elsif ($0 =~ /registration$/) {
    $ReaderMode = "One-Shot";
    printf STDERR "One-Shot mode\n";
}

elsif ($0 =~ /periodic$/) {
    $ReaderMode = "Immediate";
    printf STDERR "Immediate mode\n";
}
elsif ($0 =~ /lowduty$/) {
    $ReaderMode = "Low-Duty";
    printf STDERR "Low-Duty mode\n";
}
elsif ($0 =~ /oneshot$/) {
    $ReaderMode = "One-Shot";
    printf STDERR "One-Shot mode\n";
}
elsif ($0 =~ /testcsv$/) {
    $ReaderMode = "Test-CSV";
    printf STDERR "Test-CSV mode\n";
}


#while (@ARGV and $ARGV[0] =~ /^-/) {
#    $_ = shift;
#
#    last if /^--$/;
#    if (/^-n=(.*)/) { $ReaderName = $1 }
#}



my $RFReceiverSensitivity = 1;		# index into RF Receiver Sensitivity table
my $RFTransmitPowerMax = 53; 		# index into RF Power Table
my $ServerMode = 1;
while(1) {
    syslog('warning', sprintf("LLRP Service %s %s", $ReaderMode, $ReaderName)) if ($ServerMode);
    printf STDERR "LLRP Service %s %s\n", $ReaderMode, $ReaderName;
    service::do_service($ReaderName, $ReaderMode, $ServerMode, $DBSql, $RFTransmitPowerMax, $RFReceiverSensitivity);
}






pod2usage(1);

__END__


=head1 NAME

llrpdb - update Race RFID tags 


=head1 SYNOPSIS

llrpdb [-v] command [opts]

 Commands:
   -help                help  
   -man                 man page  
   -debug               debug messages
   -readername          reader name
   -lapdpath            Lapd directory pathname
   -racedbpath          RaceDB SQLite database pathname
   -testcsv             drive from raw csv

=head1 OPTIONS

=over 8 

=item B<-help>
Print help message

=item B<-debug>
Print debug messages

=item B<-verify>
Verify Race tag EPC code

=back

=cut
